import java.util.*;
public class Simdish {

	public static void main(String[] args) {
		int count=0;
		ArrayList<String> ar= new ArrayList<String>();
		String[] firstdish = new String[4];
		String[] seconddish = new String[4];
		Scanner sc = new Scanner(System.in);
		int t=sc.nextInt();
		String[] arr=new String[t];
		for(int i=0;i<t;i++){
			for(int j=0;j<4;j++){
				firstdish[j]=sc.next();
			}
			for(int j=0;j<4;j++){
				seconddish[j]=sc.next();
			}
			for(int k=0;k<4;k++){
				for(int j=0;j<4;j++){
					if(firstdish[k].equals(seconddish[j])){
						count++;
					}
				}
			}
			if(count>=2){
				ar.add("similar");
			}else{
				ar.add("dissimilar");
			}
			count=0;
		}
		Iterator itr=ar.iterator(); 
		while(itr.hasNext()){  
			System.out.println(itr.next());  
		}  
	}
		
}
