import java.util.*;
public class Snelect {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int t=sc.nextInt();
		ArrayList<String> ar = new ArrayList<String>();
		for(int i=0;i<t;i++){
			ar.add(sc.next());
 		}
		for(String s : ar){
			char arr[]=s.toCharArray();
			int count_s=0,count_m=0;
			for(int i=0;i<s.length();i++){
				if(arr[i]=='s'){
					count_s++;
				}else if(arr[i]=='m'){
					count_m++;
				}
			}
			for(int i=0;i<s.length();){
				if(i==s.length()-1)
					break;
				if(arr[i]=='s'&&arr[i+1]=='m'){
					count_s--;
					i=i+2;
				}
				else if(arr[i]=='m'&&arr[i+1]=='s'){
					count_s--;
					i=i+2;
				}
				else{
					i++;
				}
			}
			if(count_s>count_m){
				System.out.println("snakes");
			}else if(count_s<count_m)
			{
				System.out.println("mongooses");
			}else{
				System.out.println("tie");
			}
		}
	}

}
