import java.util.*;

public class TempleLand {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int t, n, count = 0, k, middle;
		t = sc.nextInt();
		for (int i = 0; i < t; i++) {
			n = sc.nextInt();
			count=0;
			int arr[] = new int[n];
			for (int j = 0; j < n; j++) {
				arr[j] = sc.nextInt();
			}
			if (n % 2 == 0) {
				System.out.println("no");
			} else {
				middle = n / 2;
				k = middle - 1;
				/*for(int j=0;j<n;j++){
					if(arr[j]==arr[middle]){
						System.out.println("no");
						return;
					}
				}*/
				//System.out.println(arr[middle]);
				for (int j = 0; j < n; j++) {
					if (arr[j] == j + 1 && j <=middle) {
						count++;
					}
					if (arr[j] == arr[k] && j > middle) {
						count++;
						k--;
					}
				}
				if (count == n) {
					System.out.println("yes");
				} else {
					System.out.println("no");
				}
			}
		}
	}

}
