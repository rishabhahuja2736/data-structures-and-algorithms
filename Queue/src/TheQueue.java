//http://www.geeksforgeeks.org/data-structures/
public class TheQueue {
	static final int Max = 1000;
	int front, rear;
	int arr[] = new int[Max];

	TheQueue() {
		front = -1;
		rear = -1;
	}

	public boolean isEmpty() {
		if (rear == -1 && front == -1) {
			return true;
		} else {
			return false;
		}
	}

	public void enqueue(int x) {
		if (rear == Max - 1) {
			System.out.println("Queue is full");
			return;
		} else if ((rear + 1) % Max == front) {
			return;
		} else if (isEmpty()) {
			rear = front = 0;
			arr[rear] = x;
		} else {
			arr[rear] = x;
		}
		rear = (rear + 1) % Max;
	}

	public int dequeue() {
		if (isEmpty()) {
			System.out.println("Queue is Empty");
			return 0;
		} else if (front == rear) {
			return arr[front];
		} else {
			int x = arr[front];
			front = (front + 1) % Max;
			return x;
		}
	}
	
	public void print(){
		for(int i=rear-1;i>=front;i--){
			System.out.println(arr[i]);
		}
	}
	public static void main(String args[]){
		TheQueue q = new TheQueue();
		System.out.println("Intial queue");
		q.enqueue(2);
		q.enqueue(4);
		q.enqueue(6);
		q.enqueue(8);
		q.print();
		System.out.println("Queue after removal");
		q.dequeue();
		q.enqueue(10);
		q.print();
	}
}