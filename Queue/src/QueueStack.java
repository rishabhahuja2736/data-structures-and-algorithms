//http://www.geeksforgeeks.org/?p=5009
import java.util.*;

public class QueueStack {
	Stack<Integer> s1 = new Stack<>();
	Stack<Integer> s2 = new Stack<>();
	public void enqueue(int data) {
		s1.push(data);
	}

	public void dequeue() {
		if (s1.isEmpty() && s2.isEmpty()) {
			System.out.println("Invalid");
			return;
		}
		while (!s1.isEmpty()) {
			//System.out.println("Hello");
			s2.push(s1.pop());
		}
		System.out.println(s2.pop());
	}

	public static void main(String args[]) {
		QueueStack q = new QueueStack();
		q.enqueue(2);
		q.enqueue(4);
		q.enqueue(6);
		q.enqueue(8);
		q.dequeue();
		q.dequeue();

	}
}
