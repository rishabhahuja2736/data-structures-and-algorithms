//http://www.geeksforgeeks.org/queue-set-2-linked-list-implementation/
public class QueueLl {
	Node temp = null;
	Node rear = null;
	Node front = null;

	// head=rear=front=null;
	public class Node {
		int data;
		Node next;

		Node(int data) {
			this.data = data;
		}
	}

	public void enqueue(int data) {
		Node newnode = new Node(data);
		if(rear==null){
			rear=newnode;
			front=rear;
			return;
		}
		else{
			rear.next=newnode;
			rear=newnode;
		}
	}
	public void dequeue(){
		front=front.next;
	}
	public void printll(){
		temp=front;
		while(temp!=null){
			System.out.println(temp.data);
			temp=temp.next;
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		QueueLl q =new QueueLl();
		System.out.println("Intial queue");
		q.enqueue(2);
		q.enqueue(4);
		q.enqueue(6);
		q.enqueue(8);
		q.printll();
		System.out.println("Queue after removal");
		q.dequeue();
		q.printll();
	}

}
