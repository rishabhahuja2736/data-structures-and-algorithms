package com.luv2code.trees;

import java.util.Scanner;
import com.luv2code.trees.BinaryTree.*;
public class NoOfNodes {
	/*class Node{
		int data;
		Node left;
		Node right;
		Node(int data){
			this.data=data;
			left=null;
			right=null;
		}
	}
	NoOfNodes(int data){
		Scanner sc = new Scanner(System.in);
		root=insertNode(sc,null);
	}
	Node root;
	public Node insertNode(Scanner sc,Node parent){
		if(parent==null){
			System.out.println("Enter data");
		}
		int data=sc.nextInt();
		parent = new Node(data);
		System.out.println("Do you want left of "+parent.data);
		char ch1=sc.next().charAt(0);
		if(ch1=='y'){
			parent.left=insertNode(sc,parent.left);
		}
		System.out.println("Do you want right of "+parent.data);
		char ch2=sc.next().charAt(0);
		if(ch2=='y'){
			parent.right=insertNode(sc,parent.right);
		}
		return parent;
	}*/
	public void nodes(){
		BinaryTree b = new BinaryTree();
		System.out.println(nodes(b.root));
	}
	public static int nodes(Node root){
		if(root!=null){
			int l=nodes(root.left);
			int r=nodes(root.right);
			return 1+l+r;
		}else{
			return 0;
		}
	}
	public void display(Node node){
		if(node!=null){
			System.out.println(node.data);
			display(node.left);
			display(node.right);
		}
	}
	

}
