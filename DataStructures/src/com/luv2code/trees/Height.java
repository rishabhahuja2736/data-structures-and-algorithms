package com.luv2code.trees;

import com.luv2code.trees.BinaryTree.*;

public class Height {
	public void height(){
		BinaryTree b = new BinaryTree();
		System.out.println(height(b.root));
	}
	private static int height(Node node){
		if(node==null)
			return 0;
		else{
			int l=height(node.left);
			int r=height(node.right);
			return 1+Math.max(l,r);
		}
	}
}
