package com.luv2code.trees;

import java.util.Scanner;

public class BinaryTree {
	public class Node{
		Node left;
		Node right;
		int data;
		Node(int data){
			this.data=data;
			left=null;
			right=null;
		}
	}
	Node root;
	BinaryTree(){
		Scanner sc = new Scanner(System.in);		
		root =insertNode(sc,null);
	}
	
	private Node insertNode(Scanner sc,Node parent){
		if(parent==null){
			System.out.println("Enter data");
		}
		int data=sc.nextInt();
		parent=new Node(data);
		System.out.println("Do you want left of "+parent.data);
		char ch=sc.next().charAt(0);
		if(ch=='y'){
			parent.left=insertNode(sc,parent.left);
		}
		System.out.println("Do you want right of "+parent.data);
		char ch2=sc.next().charAt(0);
		if(ch2=='y'){
			parent.right=insertNode(sc,parent.right);
		}
		return parent;
		
	}
	public void display(){
		this.display(this.root);
	}
	
	public Node returnBinaryTree(){
		return root;
	}
	private void display(Node root){
		if(root!=null){
			System.out.print(root.data+" ");
			display(root.left);
			display(root.right);
		}
	}
}
