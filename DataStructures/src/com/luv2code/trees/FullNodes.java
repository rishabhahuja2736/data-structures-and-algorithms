  package com.luv2code.trees;

import com.luv2code.trees.BinaryTree.Node;

public class FullNodes {
	public void fullNodes(){
		BinaryTree b = new BinaryTree();
		FullNodes f = new FullNodes();
		System.out.println(f.fullNodes(b.root));
	}
	private int fullNodes(Node node){
		if(node==null)
			return 0;
		else{
			int l=fullNodes(node.left);
			int r=fullNodes(node.right);
			if(node.left!=null&&node.right!=null){
				return 1+l+r;
			}
			return l+r;
		}
	}
}
