package com.luv2code.trees;
import com.luv2code.trees.BinaryTree.*;

public class NoOfLeaves {
	public void leaves(){
		BinaryTree b = new BinaryTree();
		System.out.println(leaves(b.root));
	}
	private static int leaves(Node root){
		if(root==null)
			return 0;
		else{
			int l=leaves(root.left);
			int r=leaves(root.right);
			if(root.left==null&&root.right==null){
					return 1+l+r;
			}
			return l+r;
		}		
	}
	/*public static void main(String[] args) {
		
		System.out.println(leaves(b.root));
	}*/

}
