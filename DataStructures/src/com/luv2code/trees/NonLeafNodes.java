package com.luv2code.trees;

import com.luv2code.trees.BinaryTree.Node;

public class NonLeafNodes {
	public void nonLeaf(){
		BinaryTree b = new BinaryTree();
		System.out.println(nonLeafNodes(b.root));
	}
	private static int nonLeafNodes(Node node){
		if(node==null)
			return 0;
		else{
			int l=nonLeafNodes(node.left);
			int r=nonLeafNodes(node.right);
			if(node.left==null&&node.right==null)
				return l+r;
			return 1+l+r;
		}
	}
	
}
