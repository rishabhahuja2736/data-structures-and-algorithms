package com.luv2code.linkedlist;
public class ReverseLLIterativeRecursive {
	Node head;
	class Node{
		int data;
		Node next;
		Node(int data){
			this.data=data;
			this.next=null;
		}
	}
	
	public void insertEnd(int data){
		Node newnode = new Node(data);
		if(head==null){
			head=newnode;
			return;
		}
		Node temp=head;
		while(temp.next!=null){
			temp=temp.next;
		}
		temp.next=newnode;
	}
	
	public void traverse(){
		Node temp=head;
		while(temp!=null){
			System.out.print(temp.data+"\t");
			temp=temp.next;
		}
	}
	
	public void reverse(){
		Node current=head;
		Node prev=null;
		Node next=null;
		while(current!=null){
			next=current.next;
			current.next=prev;
			prev=current;
			current=next;
		}
		head=prev;
	}
	
	public void reverseRecursive(Node prev,Node current){
		if(current!=null){
			reverseRecursive(current,current.next);
			current.next=prev;
		}
		else{
			head=prev;
		}
	}
	public static void main(String[] args){  
		ReverseLLIterativeRecursive r =new ReverseLLIterativeRecursive();
		r.insertEnd(1);
		r.insertEnd(2);
		r.insertEnd(3);
		r.insertEnd(4);
		r.traverse();
		//r.reverse();
		r.reverseRecursive(null, r.head);
		System.out.println();
		r.traverse();
    }      
}
