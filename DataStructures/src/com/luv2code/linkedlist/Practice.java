package com.luv2code.linkedlist;
public class Practice {
	private class Node{
		int data;
		Node next;
		Node(int data){
			this.data=data;
			next=null;
		}
	}
	Node head;
	public void insertEnd(int data){
		Node newnode = new Node(data);
		if(head==null){
			head=newnode;
			return;
		}
		Node node=head;
		while(node.next!=null){
			node=node.next;
		}
		node.next=newnode;
	}
	public void reverse(Node curr){
		Node prev=null;
		Node next=null;
		while(curr!=null){
			next=curr.next;
			curr.next=prev;
			prev=curr;
			curr=next;
		}
		head=prev;
	}
	private void recursreverse(Node prev,Node curr){
		if(curr!=null){
			recursreverse(curr,curr.next);
			curr.next=prev;
		}else{
			head=prev;
		}
	}
	public void traverse(Node node){
		while(node!=null){
			System.out.print(node.data+" ");
			node=node.next;
		}
	}
	public void recurstraverse(Node node){
		if(node!=null){
			System.out.print(node.data+" ");
			recurstraverse(node.next);
		}
	}
	public static void main(String[] args) {
		Practice p = new Practice();
		p.insertEnd(1);
		p.insertEnd(2);
		p.insertEnd(3);
		p.insertEnd(4);
		p.insertEnd(5);
		p.traverse(p.head);
		p.reverse(p.head);
		System.out.println();
		p.recurstraverse(p.head);
		System.out.println();
		p.recursreverse(null,p.head);
		p.traverse(p.head);
	}

}
