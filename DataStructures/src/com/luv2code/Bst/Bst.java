package com.luv2code.Bst;

public class Bst {
	private class Node{
		int data;
		Node left;
		Node right;
		Node(int data){
			this.data=data;
			left=right=null;
		}
	}
	Node root;
	Bst(){
		root=null;
	}
	public void addNode(int data){
		if(root==null){
			root=new Node(data);
		}else{
			this.addNode2(root,data);
		}
	}
	private void addNode2(Node node,int data){
		if(data>node.data){
			if(node.right!=null){
				addNode2(node.right,data);
			}else{
				node.right=new Node(data);
			}
		}else if(data<node.data){
			if(node.left!=null){
				addNode2(node.left,data);
			}else{
				node.left=new Node(data);
			}
		}
	}
	public static int max(Node node){
		while(node.right!=null){
			node=node.right;
		}
		return node.data;
	}
	public static int min(Node node){
		while(node.left!=null){
			node=node.left;
		}
		return node.data;
	}
	public void display(Node node){
		if(node!=null){
			display(node.left);
			System.out.println(node.data);
			display(node.right);
		}
	}
	public void delete1(Node node,int data){
		this.root=delete2(node,data);
	}
	private Node delete2(Node node,int data){
		if(node==null)
			return node;
		if(data>node.data){
			node.right=delete2(node.right,data);
			return node;
		}else if(data<node.data){
			node.left=delete2(node.left,data);
			return node;
		}else{
			if(node.left==null&&node.right==null){
				return null;
			}
			else if(node.left==null){
				return node.right;
			}
			else if(node.right==null){
				return node.left;
			}else{
				int max=max(node.left);
				node.data=max;
				node.left=delete2(node.left,max);
				return node;
			}
		}
		
	}
}
