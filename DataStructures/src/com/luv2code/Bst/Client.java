package com.luv2code.Bst;

public class Client {
	public static void main(String args[]){
		Bst b = new Bst();
		b.addNode(50);
		b.addNode(30);
		b.addNode(20);
		b.addNode(40);
		b.addNode(70);
		b.addNode(60);
		b.addNode(80);
		
		//Bst.max(b.root);
		//Bst.min(b.root);
		b.delete1(b.root, 70);
		b.display(b.root);
	}
}
