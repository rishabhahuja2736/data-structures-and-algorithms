package com.luv2code.stacks;

public class StackLl {
	public class Node{
		Node next;
		int data;
		Node(int data){
			this.data=data;
			next=null;
		}
	}
	Node head;
	private void push(int data){
		Node newnode = new Node(data);
		if(head==null)
			head=newnode;
		else{
			newnode.next=head;
			head=newnode;
		}
	}
	private int pop(){
		if(head==null){
			System.out.println("Stack Underflow");
			return 0;
		}else{
			Node temp=head;
			head=temp.next;
			temp.next=null;
			return temp.data;
		}
	}
	public static void main(String[] args) {
		StackLl s = new StackLl();
		s.push(1);
		s.push(2);
		s.push(3);
		s.push(4);
		s.push(5);
		System.out.println(s.pop());
		System.out.println(s.pop());
		System.out.println(s.pop());
		System.out.println(s.pop());
	}

}
