package com.luv2code.stacks;

public class StackArr {
	int arr[] = new int[100];
	int top=-1;
	private void push(int data){
		if(top==arr.length-1){
			System.out.println("Stack Overflow");
			return;
		}
		top=top+1;
		arr[top]=data;
	}
	private int pop(){
		if(top==-1){
			System.out.println("");
			return -1;
		}
		return arr[top--];
	}
	public static void main(String[] args) {
		StackArr s = new StackArr();
		s.push(1);
		s.push(2);
		s.push(3);
		s.push(4);
		s.push(5);
		System.out.println(s.pop());
		System.out.println(s.pop());
		System.out.println(s.pop());
	}

}
