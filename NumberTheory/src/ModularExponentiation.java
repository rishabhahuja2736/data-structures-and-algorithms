//http://www.geeksforgeeks.org/modular-exponentiation-power-in-modular-arithmetic/
import java.util.*;
public class ModularExponentiation {
	public static int modulopower(int x,int y,int n){
		int result=1;
		x=x%n;
		while(y>0){
			if(y%2!=0){
				result=(result*x)%n;
			}
			y=y/2;
			x=(x*x)%n;
		}
		return result;
	}
	public static void main(String args[]){
		ModularExponentiation m = new ModularExponentiation();
		int x,y,n;
		Scanner sc = new Scanner(System.in);
		x=sc.nextInt();
		y=sc.nextInt();
		n=sc.nextInt();
		System.out.println(modulopower(x, y, n));
	}
}
