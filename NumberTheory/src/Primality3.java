import java.util.Random;

public class Primality3 {
	public static int modulopower(int x,int y,int n){
		int result=1;
		x=x%n;
		while(y>0){
			if(y%2!=0){
				result=(result*x)%n;
			}
			y=y/2;
			x=(x*x)%n;
		}
		return result;
	}
	public static boolean millerTest(int n,int d){
		Random rand = new Random();
		int randno=rand.nextInt();
		int x=modulopower(randno,d,n);
		if(x==1||x==n-1){
			return true;
		}
		while(n!=n-1){
			x=(x*x) % n;
			if(x==1)
				return false;
			if(x==n-1){
				return true;
			}
		}
		return false;
	}
	public static boolean isPrime(int n,int k){
		if(n==1||n==0)
			return false;
		if(n<=3)
			return true;
		if(n%2==0)
			return false;
		int d=n-1;
		for(int i=0;i<k;i++){
			if(millerTest(d,k)==false){
				return false;
			}
		}
		return true;	
	}
	public static void main(String args[]){
		Primality3 p = new Primality3();
		System.out.println(isPrime(1,5));
	}
}
