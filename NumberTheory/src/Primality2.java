import java.util.Random;
import java.util.Scanner;

public class Primality2 {
	public static long modulopower(long x,long y,long n){
		long result=1;
		x=x%n;
		while(y>0){
			if(y%2!=0){
				result=(result*x)%n;
			}
			y=y/2;
			x=(x*x)%n;
		}
		return result;
	}
	public static boolean fermat(long n,long iter){
		if (n <= 1 || n == 4)  return false;
		if (n <= 3) return true;
		 
		Random rand = new Random();
		for(int i=1;i<=iter;i++){
			long randno=Math.abs(rand.nextLong());
			//long no=2 + randno%(n-4);
			long no=1+randno%(n-1);
			if(modulopower(no,n-1,n)!=1){
				return false;
			}
			System.out.println("Hello");
		}
		return true;
	}
	public static void main(String args[]){
		Scanner sc = new Scanner(System.in);
		long n=sc.nextLong();
		long iter=sc.nextLong();
		System.out.println(fermat(n,iter));
	}
}
