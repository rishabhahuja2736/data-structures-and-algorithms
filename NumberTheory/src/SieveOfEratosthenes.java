//http://www.geeksforgeeks.org/sieve-of-eratosthenes/
import java.util.*;
public class SieveOfEratosthenes {
	public static boolean[] sieve(int n){
		int finallist[]=new int[n+1];
		boolean list[] = new boolean[n+1];
		for(int i=0;i<n;i++){
			list[i]=true;
		}
		list[0]=false;
		list[1]=false;
		for(int i=2;i<Math.sqrt(n);i++){
			for(int j=2;j*i<=n;j++){
				if(list[i]==true)
					list[j*i]=false;
			}
		}
		return list;
	}
	public static void main(String args[]){
		SieveOfEratosthenes s =new SieveOfEratosthenes();
		Scanner sc =new Scanner(System.in);
		int n=sc.nextInt();
		boolean a[] = new boolean[n+1];
		a=sieve(n);
		for(int i=0;i<=n;i++){
			if(a[i]==true)
			System.out.println(i);
		}
	}
}
