package basics;
import java.util.Scanner;

//import java.util.Scanner;

public class transpose {

	public static void main(String[] args)
	{
	
	int r,c,m,n,j,i,temp;

	
	Scanner sc = new Scanner(System.in);
	System.out.println("enter the no of rows");
	r=sc.nextInt();

	System.out.println("enter the no of coloumns");
	c=sc.nextInt();
	
	int a[][] = new int[r+1][c+1];
	System.out.println("enter the 2-d matrix");
	for(i=1;i<=r;i++)
	{
	
		for(j=1;j<=c;j++)
		{
			
			a[i][j]=sc.nextInt();	
		}
		
	}
	

	System.out.println("Matrix before transpose is as follows:-");
	for(i=1;i<=r;i++)
	{
		for( j=1;j<=c;j++)
		{
			System.out.print(a[i][j]+"\t");
		}
		System.out.println();
	}

	System.out.println("Transpose matrix is as follows:-");
	for( i=1;i<=c;i++)
	{
		for( j=1;j<=r;j++)
		{
			System.out.print(a[j][i]+"\t");
		}
		System.out.println();
	}
}
}
