package basics;
import java.util.Scanner;

public class table {

	public static void main(String args[])
	{
		int num,upto;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the no whose table is to be printed");
		num=sc.nextInt();
		System.out.println("Enter the no upto which table is to be printed");
		upto=sc.nextInt();
		for(int i=1;i<=upto;i++)
		{
			System.out.println(num+" * "+i+" = " + num*i);
			System.out.println();
		}

	}

}
