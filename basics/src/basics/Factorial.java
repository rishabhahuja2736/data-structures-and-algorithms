package basics;

import java.util.Scanner;

public class Factorial {
	public static void main(String[] args)
	{
		int num;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("enter the number");
		num=sc.nextInt();
		for(int i=num-1;i>=1;i--)
		{
			num = num*i;
		}
		System.out.println("factorial of number is "+num);
	}

}
