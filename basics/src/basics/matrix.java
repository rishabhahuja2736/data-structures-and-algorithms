package basics;
import java.util.Scanner;

public class matrix {

	public static void main(String[] args)
	{
		
		int r,c;
		int a[][] = new int[3][3];
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the no of rows");
		r=sc.nextInt();

		System.out.println("enter the no of coloumns");
		c=sc.nextInt();
		
		System.out.println("enter the 2-d matrix");
		for(int i=1;i<=r;i++)
		{
			for(int j=1;j<=c;j++)
			{
				a[i][j]=sc.nextInt();
				
			}
		}
		System.out.println("matrix is as follows:-");
		for(int i=1;i<=r;i++)
		{
			for(int j=1;j<=c;j++)
			{
				System.out.print(a[i][j]+"\t");
			}
			System.out.println();
		}
	}
}
