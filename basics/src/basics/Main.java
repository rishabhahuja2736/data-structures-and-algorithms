package basics;
import java.io.*;
import java.util.*;
public class Main {

	public static void main(String[] args) throws java.lang.Exception{
		int n,k,count=0,t;
		java.io.BufferedReader br = new java.io.BufferedReader(new java.io.InputStreamReader(System.in));
		n=Integer.parseInt(br.readLine());
		k=Integer.parseInt(br.readLine());
		ArrayList<Integer> al=new ArrayList<Integer>();
		for(int i=0;i<n;i++){
			t=Integer.parseInt(br.readLine());	
			al.add(t);
		}
		for(int i=0;i<n;i++){
			if(al.get(i)%k==0){
				count++;
			}
		}
		System.out.println(count);	
	}	
}
