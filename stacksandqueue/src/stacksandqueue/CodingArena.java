package stacksandqueue;
import java.util.Scanner;

public class CodingArena {
	public void test(){
		int n,k,initial,sum=0;
		Scanner sc = new Scanner(System.in);
		n=sc.nextInt();
		k=sc.nextInt();
		int[] a = new int[n];
		int[] b = new int[n];
		int[] c = new int[2*n];
		for(int i=0;i<n;i++){
			a[i]=sc.nextInt();
		}
		for(int i=0;i<n;i++){
			b[i]=sc.nextInt();
		}
		for(int i=0;i<n;i++){
			initial=a[i];
			a[i]=a[i]+(2*k);
			for(int j=0;j<n;j++){
				sum=sum+(a[j]*b[j]);
			}
			c[i]=sum;
			a[i]=initial;
			sum=0;
		}
		for(int i=0;i<n;i++){
			initial=a[i];
			a[i]=a[i]-(2*k);
			for(int j=0;j<n;j++){
				sum=sum+(a[j]*b[j]);
			}
			c[n+i]=sum;
			a[i]=initial;
			sum=0;
		}
		int min = getMin(c);
	    System.out.println(min); 
	}
	public int getMin(int[] inputArray){ 
	    int minValue = inputArray[0]; 
	    for(int i=1;i<inputArray.length;i++){ 
	      if(inputArray[i] < minValue){ 
	        minValue = inputArray[i]; 
	      } 
	    } 
	    return minValue; 
	  }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CodingArena c = new CodingArena();
		c.test();

	}

}
