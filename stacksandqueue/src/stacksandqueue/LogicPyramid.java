package stacksandqueue;
import java.util.Scanner;

public class LogicPyramid{

	public void test(){
		int n,a=22,b=6,length;
		String z ;
		
		Scanner sc = new Scanner(System.in);
		n=sc.nextInt();
		for(int i=0;i<n;i++){
			for(int j=n;j>(i+1);j--)
			{
				System.out.print(" ");
			}
			for(int j=0;j<=i;j++){
				length = (int)(Math.log10(a)+1);
				if(length<5){
					z=String.format("%05d", b);
					System.out.print(z);
				}
				else{
					System.out.print(b);
				}
				b=b+a;
				a=a+16;
				System.out.print(" ");
			}
			System.out.println();
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LogicPyramid l = new LogicPyramid();
		l.test();
	}

}
