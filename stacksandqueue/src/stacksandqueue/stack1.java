//implementation of stack using linked list
package stacksandqueue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class stack1 {

	node first = null;

	class node {
		String item;
		node next;
	}

	public void push(String item) {
		node old;
		old = first;
		first = new node();
		first.next = old;
		first.item = item;
	}

	public void pop() {
		String item = first.item;
		first = first.next;
		System.out.println(item);

	}

	public boolean isEmpty() {
		return first == null;
	}

	public static void main(String args[]) throws IOException {
		stack1 st = new stack1();
		InputStreamReader r = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(r);
		String s = br.readLine();
		int len = s.split("\\w+").length;
		int i = 0;
		while (len > 0) {
			String[] tokens = s.split("\\s+");
			String var = tokens[i];
			st.push(var);
			i++;
			len--;
		}
		len = s.split("\\w+").length;
		while (len > 0) {
			st.pop();
			len--;
		}
		/*
		 * while(!(sc.nextLine().isEmpty())) { String s=sc.nextLine();
		 * 
		 * /*if(s.equals("-")){ st.pop(); }else{
		 */
		// st.push(s);
		// }
		/*
		 * } while(!(st.isEmpty())) { System.out.print(st.pop()); }
		 */
		/*
		 * st.push("all"); st.push("is"); st.push("well");
		 * System.out.println(st.pop()); System.out.println(st.pop());
		 * System.out.println(st.pop());
		 */
	}

}
