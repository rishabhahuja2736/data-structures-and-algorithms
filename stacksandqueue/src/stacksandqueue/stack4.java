//generic implementation of stack
package stacksandqueue;

import java.util.Scanner;

public class stack4<item> {
	node first=null;
	class node{
		node next;
		item sub;
		
	}
	public void push(item sub){
		node old;
		old = first;
		first=new node();
		first.sub=sub;
		first.next=old;
	}
	public item pop(){
		item sub = first.sub;
		first = first.next;
		return sub;
	}

	public static void main(String[] args) {
		stack4<Integer> st = new stack4<Integer>();
		Scanner sc =new Scanner(System.in);
		st.push(12);
		st.push(13);
		st.push(14);
		System.out.println(st.pop());
		System.out.println(st.pop());
		System.out.println(st.pop());
		
	}

}
