package stacksandqueue;
import java.util.*;
public class InbuiltStack {
	public void test(){
		Stack<String> s = new Stack<String>();
		s.push("rishabh");
		s.push("ahuja");
		s.push("doing");
		s.push("btech");
		if(!s.isEmpty()){
			s.pop();
		}
		s.push("btech");
		if(!s.isEmpty()){
			System.out.println("top of the stack:-"+s.peek());
		}else{
			System.out.println("Stack is empty");
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		InbuiltStack i = new InbuiltStack();
		i.test();
	}

}
