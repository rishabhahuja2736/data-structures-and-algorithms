//implementation of stack using array demonstrating resizing of array
package stacksandqueue;

import java.util.Scanner;

public class stack2 {
	
	String[] s;
	int n=0;
	stack2(){
		s=new String[1];
	}
	public void push(String item)
	{
		if(n==s.length)resize(2*s.length);
		s[n++]=item;		
	}
	public String pop(){
		if(n==(1/4)*s.length)resize((1/2)*s.length);
		String item = s[--n];
		s[n]=null;
		return item;
		
	}
	public void resize(int size1){
		String[] copy = new String[size1];
		for(int i=0;i<n;i++){
			copy[i]=s[i];
		}
		s=copy;
	}
	public boolean isEmpty(){
		return n==0;
	}
	public static void main(String args[]) {
		
		Scanner sc = new Scanner(System.in);
		stack2 s2 = new stack2();
		s2.push("all");
		s2.push("is");
		s2.push("well");
		while(!s2.isEmpty()){
			//st.pop();
			System.out.println(s2.pop());
		}
	}
}
