import java.util.*;
public class Kmp {
	public static int[] patternarray(char pattern[]){
		int [] lps = new int[pattern.length];
        int j =0;
        for(int i=1; i < pattern.length;){
            if(pattern[i] == pattern[j]){
                lps[i] = j + 1;
                j++;
                i++;
            }else{
                if(j != 0){
                    j = lps[j-1];
                }else{
                    lps[i] =j;
                    i++;
                }
            }
        }
        return lps;
	}
	public static boolean substringmatch(char[] text,char[] pattern){
		int[] lps=patternarray(pattern);
		int i,j;
		for(i=0,j=0;i<text.length&&j<pattern.length;){
			if(pattern[j]==text[i]){
				i++;
				j++;
			}else{
				if(j!=0){
					j=lps[j-1];
				}else{
					i++;
				}
			}
		}
		if(j==pattern.length){
			return true;
		}
		return false;
	}
	public static void main(String args[]){
		/*Scanner sc =new Scanner(System.in);
		int n=sc.nextInt();
		char[] arr = new char[n];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.next().charAt(0);
		}
		patternarray(arr);
		for(char c:arr){
			System.out.println(c+" ");
		}*/
		Scanner sc = new Scanner(System.in);
		String text,pattern;
		text=sc.nextLine();
		pattern=sc.nextLine();
		System.out.println(substringmatch(text.toCharArray(),pattern.toCharArray()));
	}
}
