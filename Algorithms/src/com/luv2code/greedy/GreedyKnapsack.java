package com.luv2code.greedy;

import java.util.Arrays;
import java.util.Comparator;

public class GreedyKnapsack {
	static class Item{
		int profit;
		int weight;
		Item(int profit,int weight){
			this.profit=profit;
			this.weight=weight;
		}
	}
	static double fractionalKnapsack(int m,Item arr[],int n){
		double profit=0;
		Arrays.sort(arr,new Comparator<Item>(){

			@Override
			public int compare(Item o1, Item o2) {
				return (int)((o2.profit/o2.weight)-(o1.profit/o1.weight));
			}	
		});
		for(int i=0;i<n;i++){
			System.out.println(arr[i].profit+" "+arr[i].weight);
		}
		for(int i=0;i<n;i++){
			int a=arr[i].weight;
			if(m>0&&arr[i].weight<=m){
				profit=profit+arr[i].profit;
				m=m-arr[i].weight;
			}
			else if(m>0){
				profit=profit+arr[i].profit*((double)m/arr[i].weight);
			}
		}
		return profit;
	}
	public static void main(String args[]){
		GreedyKnapsack g = new GreedyKnapsack();
		Item arr[]={new Item(10,2),new Item(5,3),new Item(15,5),new Item(7,7),new Item(6,1),new Item(18,4),new Item(3,1)};
		int m=15;
		int n=7;
		System.out.println(fractionalKnapsack(m, arr, n));
	}
}
