package com.luv2code.dynamic;

public class Lcs {
	public int leastCommonSubsequence(char x[], char y[]) {
		int m = x.length;
		int n = y.length;
		
		int c[][] = new int[m+1][n+1];

		for (int i = 0; i <= m; i++) {
			for (int j = 0; j <= n; j++) {
				if(i==0||j==0){
					c[i][j]=0;
				}else if (x[i-1] == y[j-1]) {
					c[i][j] = 1 + c[i - 1][j - 1];
				} else {
					c[i][j] = Math.max(c[i - 1][j], c[i][j - 1]);
				}
			}
		}
		return c[m][n];
	}

	public static void main(String[] args) {
		Lcs l = new Lcs();
		
		String s1="ABCBDAB";
		String s2 = "BDCABA";
		 
	    char[] X=s1.toCharArray();
	    char[] Y=s2.toCharArray();
	    
	    System.out.println(l.leastCommonSubsequence(X, Y));
		
	}

}
