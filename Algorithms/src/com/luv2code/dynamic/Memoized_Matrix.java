package com.luv2code.dynamic;

public class Memoized_Matrix {
	public int Memoized_matrix(int p[]){
		int n=p.length-1;
		int m[][]= new int[n+1][n+1];
		for(int i=1;i<=n;i++){
			for(int j=1;j<=n;j++){
				m[i][j]=Integer.MAX_VALUE;
			}
		}
		return Lookup_Chain(m,p,1,n);
	}
	public int Lookup_Chain(int m[][],int p[],int i,int j){
		int q;
		//System.out.println(i+" and "+j);
		if(m[i][j]<Integer.MAX_VALUE)
			return m[i][j];
		if(i==j){
			m[i][j]=0;
		}
		else{
			for(int k=i;k<=j-1;k++){
				q=Lookup_Chain(m,p,i,k)+Lookup_Chain(m,p,k+1,j)+p[i-1]*p[k]*p[j];
				if(q<m[i][j]){
					m[i][j]=q;
				}
			}
		}
		return m[i][j];
	}
	public static void main(String[] args) {
		int arr[] = { 1, 2, 1, 4, 1 };
		Memoized_Matrix m = new Memoized_Matrix();
		System.out.println(m.Memoized_matrix(arr));
	}
}
