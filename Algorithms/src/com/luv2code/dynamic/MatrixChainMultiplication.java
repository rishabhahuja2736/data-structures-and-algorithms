package com.luv2code.dynamic;

public class MatrixChainMultiplication {
	public int matrix_chain(int p[]) {
		int n = p.length-1;
		int m[][] = new int[n+1][n+1];
		int s[][] = new int[n+1][n+1];
		int j, q;
		for (int i = 1; i < n; i++) {
			m[i][i] = 0;
		}
		for (int l = 2; l <= n; l++) {
			for (int i = 1; i <= n - l + 1; i++) {
				j = i + l - 1;
				m[i][j] = Integer.MAX_VALUE;
				for (int k = i; k <= j - 1; k++) {
					q = m[i][k] + m[k + 1][j] + p[i - 1] * p[k] * p[j];
					if (q < m[i][j]) {
						m[i][j] = q;
						s[i][j] = k;
					}
				}
			}
		}
		return m[1][n];
	}

	public static void main(String args[]) {
		int arr[] = { 1, 2, 1, 4, 1 };
		MatrixChainMultiplication m = new MatrixChainMultiplication();
		System.out.println(m.matrix_chain(arr));
	}
}
