/*
https://www.youtube.com/watch?v=AaJlOgUWBJQ
http://www.geeksforgeeks.org/check-if-a-number-can-be-expressed-as-xy-x-raised-to-power-y/
 */
import java.util.*;
public class PowerOfTwoIntegers {
	double y;
	public boolean isPower(int a){
		if(a==1)
			return true;
		for(int x=2;x<=Math.sqrt(a);x++){
			y=(Math.log(a)/Math.log(x));
			//System.out.println(y);
			if ((y - (int) y) < 0.000000001) 	
				return true;
		}
		return false;
	}
	
	public static void main(String args[]){
		PowerOfTwoIntegers p = new PowerOfTwoIntegers();
		Scanner sc = new Scanner(System.in);
		int n=sc.nextInt();
		System.out.println(p.isPower(n));
	}
}

