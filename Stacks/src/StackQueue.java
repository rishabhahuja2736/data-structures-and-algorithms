//http://www.geeksforgeeks.org/implement-a-stack-using-single-queue/
import java.util.*;

public class StackQueue {
	static Queue<Integer> q = new LinkedList<Integer>();

	public void push(int data) {
		int size = q.size();
		q.add(data);
		for (int i = 0; i < size; i++) {
			q.add(q.remove());
		}
	}

	public void pop() {
		System.out.println(q.remove());
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		StackQueue s = new StackQueue();
		s.push(10);
		s.push(20);
		s.push(30);
		s.push(40);
		s.pop();
		s.pop();
	}

}
