//http://www.geeksforgeeks.org/stack-set-4-evaluation-postfix-expression/
import java.util.*;
public class PostfixEvaluate {
	static int Max=1000;
	int arr[] = new int[Max];
	int top;
	int ele;
	PostfixEvaluate(){
		top=-1;
	}
	public void value(char c){
		int i;
		switch(c){
			case '+':
				i=pop()+pop();
				push(i);
				break;
			case '-':
				i=pop()-pop();
				push(i);
				break;	
			case '*':
				i=pop()*pop();
				push(i);
				break;
			case '/':
				i=pop()-pop();
				push(i);
				break;
			case '^':
				i=pop()^pop();
				push(i);
				break;
		}
	}
	public void evaluate(String s){
		for(int i=0;i<s.length();i++){
			char c=s.charAt(i);
			if(Character.isLetterOrDigit(c)){
				push(Character.getNumericValue(c));
				//System.out.println("hello");
			}else{
				value(c);
			}
		}
	}
	public void push(int data){
		if(top>=1000){
			System.out.println("Overflow");
		}else{
			arr[++top]=data;
			//System.out.println(data);
		}
	}
	public int pop(){
		if(top<0){
			System.out.println("Stack is full");
			return 0;
		}else{
			ele=arr[top--];
			return ele;
		}
	}
	public static void main(String[] args) {
		PostfixEvaluate e = new PostfixEvaluate();
		Scanner sc = new Scanner(System.in);
		String s = sc.next();
		e.evaluate(s);
		System.out.println(e.pop());
	}

}
