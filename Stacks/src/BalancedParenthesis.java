//http://www.geeksforgeeks.org/?p=6547
import java.util.*;
public class BalancedParenthesis {
	static int Max=1000;
	char arr[] = new char[Max];
	int top;
	char ele;
	BalancedParenthesis(){
		top=-1;
	}
	
	public boolean matchingChar(char c1,char c2){
		if(c1=='{'&&c2=='}'){
			return true;
		}else if(c1=='('&&c2==')'){
			return true;
		}else if(c1=='['&&c2==']'){
			return true;
		}else{
			return false;
		}
	}
	
	public void balanced(String s){
		for(int i=0;i<s.length();i++){
			if(s.charAt(i)=='('||s.charAt(i)=='{'||s.charAt(i)=='['){
				push(s.charAt(i));
			}else if(s.charAt(i)==')'||s.charAt(i)=='}'||s.charAt(i)==']'){
				char c=pop();
				if(!matchingChar(c, s.charAt(i))){
					System.out.println("Does not Have Balanced Parenthesis");
					return;
				}
			}
		}if(!isEmpty()){
			System.out.println("Does not Have Balanced Parenthesis");
		}else{
			System.out.println("Have Balanced Parenthesis");
		}
		
	}

	public void push(char data){
		if(top>=Max){
			System.out.println("Overflow");
		}else{
			arr[++top]=data;
		}
	}
	public char pop(){
		if(top<0){
			System.out.println("Stack is Empty");
			return 0;	
		}else{
			ele=arr[top--];
			//System.out.println(ele);
			return ele;
		}
	}
	public boolean isEmpty(){
		if(top<0){
			return true;
		}else{
			return false;
		}
	}
	public static void main(String[] args) {
		BalancedParenthesis b = new BalancedParenthesis();
		Scanner sc = new Scanner(System.in);
		String s= sc.next();
		b.balanced(s);
	}
}
