//http://www.geeksforgeeks.org/?p=8405
import java.util.*;
public class NextGreater {
	static int Max=1000;
	int top;
	int ele;
	int next;
	int element;
	int arr[] = new int[Max];
	NextGreater(){
		top=-1;
	}
	public void push(int data){
		if(top>=Max){
			System.out.println("Stack is full");
		}else{
			arr[++top]=data;
			System.out.println(data);
		}
	}
	public int pop(){
		if(top<0){
			System.out.println("Stack is Empty");
			return 0;
		}else{
			ele=arr[top--];
			return ele;
		}
	}
	public boolean isEmpty(){
		if(top>0){
			return false;
		}else{
			return true;
		}
	}
	public static int x=4;
	public void nextGreater(int arr[]){
		push(arr[0]);
		for(int i=1;i<arr.length;i++){
			next=arr[i];
			element=pop();
			while(element<next){
				System.out.println(element+"->"+next);
				if(isEmpty()){
					break;
				}	
				element=pop();
				if(element>next)
					push(element);
			}
			//System.out.println("---------"+x);
			//System.out.println("--"+element);
			//System.out.println("--"+next);
			push(next);
		}
		while(!isEmpty()){
			System.out.println(pop()+"->"+"-1");
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		NextGreater ng = new NextGreater();
		Scanner sc = new Scanner(System.in);
		int n=sc.nextInt();
		int arr[]=new int[n];
		for(int i=0;i<n;i++){
			arr[i]=sc.nextInt();
		}
		ng.nextGreater(arr);
	}

}
