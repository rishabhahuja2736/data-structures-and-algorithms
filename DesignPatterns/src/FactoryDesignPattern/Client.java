package FactoryDesignPattern;

public class Client {
	public static void main(String args[]){
		OperatingFactory of =new OperatingFactory();
		OS ob =of.getInstance("open");
		ob.spec();
		ob =of.getInstance("close");
		ob.spec();
	}
}
