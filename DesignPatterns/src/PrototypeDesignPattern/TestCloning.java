package PrototypeDesignPattern;

public class TestCloning {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Sheep sally = new Sheep();
		Sheep clonedSheep=(Sheep)CloneFactory.getClone(sally);
		System.out.println(sally);
		System.out.println(clonedSheep);
		System.out.println("Sally HashCode: " + System.identityHashCode(System.identityHashCode(sally)));
		System.out.println("Clone HashCode: " + System.identityHashCode(System.identityHashCode(clonedSheep)));
	}

}
