package StrategyDesignPattern;

public class AnimalPlay {
	public static void main(String args[]){
		Dog jimmy = new Dog();
		Bird sparrow = new Bird();
		System.out.println("Dog: "+jimmy.trytofly());
		System.out.println("Bird: "+sparrow.trytofly());
		jimmy.flyingtype=new CanFly();
		System.out.println("Dog: "+jimmy.trytofly());
	}
}
