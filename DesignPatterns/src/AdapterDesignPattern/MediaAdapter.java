package AdapterDesignPattern;

public class MediaAdapter implements MediaPlayer {
	AdvancedMediaPlayer advanceMedia;
	public MediaAdapter(String audioType){
		if(audioType.equals("vlc")){
			advanceMedia=new VlcPlayer();
		}
		if(audioType.equals("mp4")){
			advanceMedia=new Mp4Player();
		}
	}
	@Override
	public void play(String audioType, String fileName) {
		// TODO Auto-generated method stub
		if(audioType.equals("vlc")){
			advanceMedia.playVlc(fileName);
		}
		if(audioType.equals("mp4")){
			advanceMedia.playMp4(fileName);
		}
	}
}
