package BridgeDesignPattern;
public class Circle extends Shape {
	int x,y,radius;
	Circle(int radius,int x,int y,DrawApi drawApi){
		super(drawApi);
		// TODO Auto-generated constructor stub
		this.radius=radius;
		this.x=x;
		this.y=y;
	}
	@Override
	public void draw() {
		// TODO Auto-generated method stub
		drawApi.drawCircle(radius,x,y);
	}
}
