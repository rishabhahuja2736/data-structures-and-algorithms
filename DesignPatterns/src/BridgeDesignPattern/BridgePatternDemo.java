package BridgeDesignPattern;

public class BridgePatternDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Circle c1 = new Circle(100,100,10,new RedCircle());
		Circle c2 = new Circle(200,200,20,new GreenCircle());
		c1.draw();
		c2.draw();
	}

}
