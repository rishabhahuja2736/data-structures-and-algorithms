package BuilderDesignPattern;

public class OldRobotBuilder implements RobotBuilder{
	private Robot robot;
	public OldRobotBuilder(){
		this.robot=new Robot();
	}
	@Override
	public void buildRobotHead() {
		// TODO Auto-generated method stub
		robot.setRobotHead("Tine Head");
	}

	@Override
	public void buildRobotTorso() {
		// TODO Auto-generated method stub
		robot.setRobotTorso("Tin Torso");
	}

	@Override
	public void buildRobotArms() {
		// TODO Auto-generated method stub
		robot.setRobotArms("BlowTorch Arms");
	}

	@Override
	public void buildRobotLegs() {
		// TODO Auto-generated method stub
		robot.setRobotLegs("Roller Skates");
	}

	@Override
	public Robot getRobot() {
		// TODO Auto-generated method stub
		return robot;
	}

}
