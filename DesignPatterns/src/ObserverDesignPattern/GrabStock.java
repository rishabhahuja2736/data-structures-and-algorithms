package ObserverDesignPattern;

public class GrabStock {
	public static void main(String args[]){
		StockGrabber stockGrabber = new StockGrabber();
		
		StockObserver observer1 = new StockObserver(stockGrabber);
		stockGrabber.setIbmPrice(197.00);
		stockGrabber.setApplPrice(655.00);
		stockGrabber.setGoogPrice(800.00);
		
		
		StockObserver observer2 = new StockObserver(stockGrabber);
		stockGrabber.setIbmPrice(250.00);
		stockGrabber.setApplPrice(550.00);
		stockGrabber.setGoogPrice(980.00);
		
		stockGrabber.unregister(observer1);
		
	}
}
