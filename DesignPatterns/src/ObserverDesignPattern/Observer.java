package ObserverDesignPattern;

public interface Observer {
	public void update(double ibmprice,double applprice,double googprice);
}
