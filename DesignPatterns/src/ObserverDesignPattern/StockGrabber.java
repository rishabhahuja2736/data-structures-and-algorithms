package ObserverDesignPattern;

import java.util.ArrayList;

public class StockGrabber implements Subject {
	private ArrayList<Observer> observers;
	private double ibmprice;
	private double applprice;
	private double googprice;
	public StockGrabber(){
		observers = new ArrayList<Observer>();
	}
	@Override
	public void register(Observer newobserver) {
		
		observers.add(newobserver);
	}
	@Override
	public void unregister(Observer deleteobserver) {
		
		int observerindex=observers.indexOf(deleteobserver);
		
		System.out.println("Observer " + (observerindex+1) + " deleted");
		observers.remove(deleteobserver);
	}
	@Override
	public void notifyobserver() {
		for(Observer observer: observers){
			observer.update(ibmprice, applprice, googprice);
		}
	}
	public void setIbmPrice(double newIbmPrice){
		ibmprice=newIbmPrice;
		notifyobserver();
	}
	public void setApplPrice(double newApplPrice){
		applprice=newApplPrice;
		notifyobserver();
	}
	public void setGoogPrice(double newGoogPrice){
		googprice=newGoogPrice;
		notifyobserver();
	}
	 
}
