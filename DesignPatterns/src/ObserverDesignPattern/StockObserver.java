package ObserverDesignPattern;

public class StockObserver implements Observer {
	private double ibmprice;
	private double applprice;
	private double googprice;
	private static int observerIdTracker = 0;

	private int observerId;

	private Subject stockGrabber;

	public StockObserver(Subject stockGrabber){
		this.stockGrabber=stockGrabber;
		
		this.observerId=++observerIdTracker;
		
		System.out.println("New Observer " + this.observerId);

		
		stockGrabber.register(this);
	}

	@Override
	public void update(double ibmprice, double applprice, double googprice) {
		
		this.ibmprice=ibmprice;
		this.applprice=applprice;
		this.googprice=googprice;
		printPrices();
	}
	public void printPrices(){
		System.out.println(observerId+"\nIBM:"+ibmprice+"\nAAPL:"+applprice+"\nGoogl"+
	googprice+"\n");
	}

}
