import java.util.*;

public class WordCount {

	public void test() {
		String text;
		text = "she she all all all she all";
		TreeMap<String, Integer> wordlist = new TreeMap<String, Integer>();
		int count = 0;
		String[] words = text.split(" ");
		Arrays.sort(words);
		String currentword, nextword = "";
		for (int i = 0; i < words.length; i++) {
			currentword = words[i];
			count++;
			if (nextword.equals(currentword) && i == words.length - 1) {
				wordlist.put(currentword, count);
			}
			if (i == words.length - 1) {
				break;
			}
			nextword = words[i + 1];
			if (!nextword.equals(currentword)) {
				wordlist.put(currentword, count);
				count = 0;
			}
		}
		Set set = wordlist.entrySet();
		Iterator i = set.iterator();
		while (i.hasNext()) {
			Map.Entry m = (Map.Entry) i.next();
			System.out.print(m.getKey() + ": ");
			System.out.println(m.getValue());
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WordCount w = new WordCount();
		w.test();
	}
}
