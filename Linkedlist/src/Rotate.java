//http:www.geeksforgeeks.org/rotate-a-linked-list/
public class Rotate {
	Node head;
	public class Node{
		int data;
		Node next;
		Node(int data){
			this.data=data;
			next=null;
		}
	}
	public void addEnd(int data){
		Node newnode = new Node(data);
		if(head==null){
			head=newnode;
			return;
		}
		Node temp=head;
		while(temp.next!=null){
			temp=temp.next;
		}
		temp.next=newnode;
	}
	public void rotate(int k){
		Node temp1=head;
		int i=1;
		while(temp1.next!=null){
			temp1=temp1.next;
		}
		//System.out.println(temp1.data);//60
		Node temp2=head;
		while(i!=k){
			temp2=temp2.next;
			i++;
		}
		//System.out.println(temp2.data);//40
		temp1.next=head;
		head=temp2.next;
		temp2.next=null;
	}
	public void printll(){
		Node temp=head;
		while(temp!=null){
			System.out.println(temp.data);
			temp=temp.next;
		}
	}
	public static void main(String args[]){
		Rotate r = new Rotate();
		r.addEnd(10);
		r.addEnd(20);
		r.addEnd(30);
		r.addEnd(40);
		r.addEnd(50);
		r.addEnd(60);
		r.printll();
		System.out.println("After rotating");
		r.rotate(4);
		r.printll();
		
	}
}
