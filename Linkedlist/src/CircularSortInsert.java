
//http://www.geeksforgeeks.org/sorted-insert-for-circular-linked-list/
import java.util.*;

public class CircularSortInsert {
	Node last;

	public class Node {
		int data;
		Node next;

		Node(int data) {
			this.data = data;
			next = null;
		}
	}

	public void insertSort(int data) {
		Node newnode = new Node(data);
		Node temp = last.next;
		if(temp.data>=data){
			newnode.next=temp;
			last.next=newnode;
		}else if(temp==last){
			insertEmpty(data);
		}else{
			
			while (temp.next != last.next && temp.next.data <= data) {
				temp = temp.next;
			}
			if(data>temp.next.data){
				insertEnd(data);
				return;
			}
			newnode.next = temp.next;
			temp.next = newnode;
		}
	}

	public void insertEmpty(int data) {
		if (last != null) {
			return;
		}
		last = new Node(data);
		last.next = last;
	}

	public void insertEnd(int data) {
		Node temp = new Node(data);
		temp.next = last.next;
		last.next = temp;
		last = temp;
	}

	public void traverse() {
		Node temp = last.next;
		while (temp != null) {
			System.out.println(temp.data);
			temp = temp.next;
			if (temp == last.next) {
				return;
			}
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CircularSortInsert c = new CircularSortInsert();
		System.out.println("Intial list");
		c.insertEmpty(1);
		c.insertEnd(2);
		c.insertEnd(3);
		c.insertEnd(5);
		c.insertEnd(7);
		c.traverse();
		System.out.println("Enter element to be inserted");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		c.insertSort(n);
		c.traverse();

	}

}
