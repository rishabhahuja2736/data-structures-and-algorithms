//http://www.geeksforgeeks.org/reverse-a-list-in-groups-of-given-size/
public class ReverseSize {
	Node head;

	public class Node {
		int data;
		Node next;

		Node(int data) {
			this.data = data;
			next = null;
		}
	}

	public void reverse(int k) {
		Node temp = head;
		Node current = head;
		Boolean flag = true;
		Boolean flag2 = true;
		Node tempcurrent;
		while (current != null) {
			Node prev = null;
			Node next = null;
			int count = 0;
			if (flag == true) {
				tempcurrent = head;
				while (tempcurrent != null && count < k) {
					next = tempcurrent.next;
					tempcurrent.next = prev;
					prev = tempcurrent;
					tempcurrent = next;
					count++;
				}
				head = prev;
				flag = false;
			} else {
				prev = null;
				next = null;
				count = 0;
				tempcurrent = current;
				while (tempcurrent != null && count < k) {
					next = tempcurrent.next;
					tempcurrent.next = prev;
					prev = tempcurrent;
					tempcurrent = next;
					count++;
				}
				if (flag2) {
					temp.next = prev;
					flag2 = false;
				} else {
					temp.next = prev;
				}
				temp = current;
			}
			current = tempcurrent;
		}
	}

	public void insertEnd(int data) {
		Node newnode = new Node(data);
		if (head == null) {
			head = newnode;
			return;
		}
		Node temp = head;
		while (temp.next != null) {
			temp = temp.next;
		}
		temp.next = newnode;
	}

	public void printll() {
		Node temp = head;
		while (temp != null) {
			System.out.println(temp.data);
			temp = temp.next;
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ReverseSize rs = new ReverseSize();
		rs.insertEnd(1);
		rs.insertEnd(2);
		rs.insertEnd(3);
		rs.insertEnd(4);
		rs.insertEnd(5);
		rs.insertEnd(6);
		rs.insertEnd(7);
		rs.insertEnd(8);
		rs.reverse(5);
		rs.printll();
	}

}
