//http://www.geeksforgeeks.org/reverse-a-doubly-linked-list/
public class ReverseDll {
	Node head;

	public class Node {
		Node next;
		Node prev;
		int data;

		Node(int data) {
			this.data = data;
			next = null;
			prev = null;
		}
	}

	public void reverse() {
		Node temp = null;
		Node current = head;
		while (current != null) {
			temp = current.prev;
			current.prev = current.next;
			current.next = temp;
			current = current.prev;
		}
		if (temp != null) {
			head = temp.prev;
		}
	}

	public void addAfter(int data) {
		Node newnode = new Node(data);
		if (head == null) {
			head = newnode;
			return;
		}
		Node temp = head;
		while (temp.next != null) {
			temp = temp.next;
		}
		temp.next = newnode;
		newnode.prev = temp;
	}

	public void printll() {
		Node temp = head;
		while (temp != null) {
			System.out.println(temp.data);
			temp = temp.next;
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ReverseDll d = new ReverseDll();
		d.addAfter(1);
		d.addAfter(2);
		d.addAfter(3);
		d.addAfter(4);
		d.printll();
		System.out.println("Reverse linked list is:");
		d.reverse();
		d.printll();
	}

}
