
public class PreviousLogic {
	Node head;

	public class Node {
		int data;
		Node next;

		Node(int data) {
			this.data = data;
			next = null;
		}
	}
	
	public void previous(){
		Node prev=null;
		Node current=head;
		Node next=null;
		while(current!=null){
			prev=current;
			System.out.println("prev "+prev.data);
			current=current.next;
			System.out.println("current "+current.data);
			if(current.next==null){
				return;
			}
		}
		head=prev;
	}
	public void insertEnd(int data) {
		Node newnode = new Node(data);
		if (head == null) {
			head = newnode;
			return;
		}
		Node temp = head;
		while (temp.next != null) {
			temp = temp.next;
		}
		temp.next = newnode;
	}

	public void printll() {
		Node temp = head;
		while (temp != null) {
			System.out.println(temp.data);
			temp = temp.next;
		}
	}
	public static void main(String args[]) {
		PreviousLogic r = new PreviousLogic();
		r.insertEnd(1);
		r.insertEnd(2);
		r.insertEnd(3);
		r.insertEnd(4);
		r.previous();
	}
}
