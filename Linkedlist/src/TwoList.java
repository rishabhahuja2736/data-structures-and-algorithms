//http://www.geeksforgeeks.org/add-two-numbers-represented-by-linked-lists/
public class TwoList {
	Node head1;
	Node head2;
	Node head3;

	static public class Node {
		int data;
		Node next;

		Node(int data) {
			this.data = data;
			next = null;
		}
	}

	public void addList(Node head1, Node head2) {
		int rem, sum = 0, i = 1, carry = 0;
		while ((head1 != null || head2 != null)) {
			sum = carry + (head1 != null ? head1.data : 0) + (head2 != null ? head2.data : 0);
			carry = sum >= 10 ? 1 : 0;
			sum=sum%10;
			if(head3==null){
				head3=new Node(sum);
				//System.out.println(head3.data);
			}else{
				insertEnd(sum,head3);
			}
			if(head1!=null){
				head1=head1.next;
			}
			if(head2!=null){
				head2=head2.next;
			}
			
		}

	}

	public void insertEnd(int data, Node head) {
		Node newnode = new Node(data);
		Node temp = head;
		while (temp.next != null) {
			temp = temp.next;
		}
		temp.next = new Node(data);
	}

	public void printll(Node head) {
		Node temp = head;
		while (temp != null) {
			System.out.println(temp.data);
			temp = temp.next;
		}
	}

	public static void main(String args[]) {
		TwoList t = new TwoList();
		t.head1 = new Node(7);
		t.head2 = new Node(8);

		t.insertEnd(5, t.head1);
		t.insertEnd(9, t.head1);
		t.insertEnd(4, t.head1);
		t.insertEnd(6, t.head1);
		t.printll(t.head1);
		// t.insertEnd(8,t.head1);
		// t.insertEnd(8,t.head1);
		// t.printll(t.head1);
		System.out.println("second list");
		t.insertEnd(4, t.head2);
		// t.insertEnd(8,t.head2);
		t.printll(t.head2);
		System.out.println("Sum");
		t.addList(t.head1, t.head2);
		t.printll(t.head3);
	}
}
