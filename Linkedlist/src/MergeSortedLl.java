//http://www.geeksforgeeks.org/merge-two-sorted-linked-lists/
public class MergeSortedLl {
	Node head, head1, head2;

	public static class Node {
		int data;
		Node next;

		Node(int data) {
			this.data = data;
			next = null;
		}
	}

	public void merge(Node head1, Node head2) {
		Node temp1 = head1;
		Node temp2 = head2;
		while (temp1 != null && temp2 != null) {
			if (temp1.data <= temp2.data) {
				if (head == null) {
					head = new Node(temp1.data);
				} else {
					insertEnd(head, temp1.data);
				}
				temp1 = temp1.next;
			} else {
				if (head == null) {
					head = new Node(temp2.data);
				} else {
					insertEnd(head, temp2.data);
				}

				temp2 = temp2.next;
			}

		}
		while (temp1 != null) {
			insertEnd(head, temp1.data);
			temp1 = temp1.next;
		}
		while (temp2 != null) {
			insertEnd(head, temp2.data);
			temp2 = temp2.next;
		}
	}

	public void insertEnd(Node head, int data) {
		Node newnode = new Node(data);
		/*
		 * if (head == null) { head = newnode; return; }
		 */
		Node temp = head;
		while (temp.next != null) {
			temp = temp.next;
		}
		temp.next = newnode;
	}

	public void printll(Node head) {
		Node temp = head;
		while (temp != null) {
			System.out.println(temp.data);
			temp = temp.next;
		}
	}

	public static void main(String[] args) {
		MergeSortedLl m = new MergeSortedLl();
		m.head1 = new Node(5);
		m.insertEnd(m.head1, 10);
		m.insertEnd(m.head1, 15);
		System.out.println("First List");
		m.printll(m.head1);
		m.head2 = new Node(2);
		m.insertEnd(m.head2, 3);
		m.insertEnd(m.head2, 20);
		System.out.println("Second List");
		m.printll(m.head2);
		System.out.println("Sorted List");
		m.merge(m.head1, m.head2);
		m.printll(m.head);
	}

}
