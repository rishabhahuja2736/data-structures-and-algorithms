//http://www.geeksforgeeks.org/linked-list-set-2-inserting-a-node/
public class Insertion {
	Node head;
	static class Node{
		int data;
		Node next;
		Node(int data){
			this.data=data;
			next=null;
		}
	}
	public void insertBegin(int data){
		Node newnode = new Node(data);
		newnode.next=head;
		head=newnode;
	}
	public void insertAfter(Node givennode,int data){
		if(head==null){
			head = new Node(data);
			return;
		}
		Node newnode =new Node(data);
		newnode.next=givennode.next;
		givennode.next=newnode;
		
	}
	public void insertEnd(int data){
		Node newnode =new Node(data);
		if(head==null){
			head = new Node(data);
			return;
		}
		//newnode.next=null;
		Node last=head;
		while(last.next!=null){
			last=last.next;
		}
		last.next=newnode;
	}
	public void printList(){
		Node loop=head;
		while(loop!=null){
			System.out.println(loop.data);
			loop=loop.next;
		}
	}
	public static void main(String args[]){
		Insertion i = new Insertion();
		i.insertEnd(1);
		i.insertEnd(2);
		i.insertEnd(3);
		i.insertEnd(4);
		i.insertBegin(0);
		i.insertAfter(i.head.next, 100);
		i.insertEnd(5);
		i.printList();
	}
}
