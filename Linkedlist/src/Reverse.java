//http://www.geeksforgeeks.org/write-a-function-to-reverse-the-nodes-of-a-linked-list/
public class Reverse {
	Node head;
	static class Node{
		int data;
		Node next;
		Node(int data){
			this.data=data;
			next=null;
		}
	}
	public void insertEnd(int data) {
		Node newnode = new Node(data);
		if (head == null) {
			head = newnode;
			return;
		}
		Node temp = head;
		while (temp.next != null) {
			temp = temp.next;
		}
		temp.next = newnode;
	}
	public void reverse(){
		Node prev=null;
		Node current=head;
		Node next=null;
		while(current!=null){
			next=current.next;
			current.next=prev;
			prev=current;
			current=next;
		}
		head=prev;
	}
	public void printll(){
		Node temp=head;
		while(temp!=null){
			System.out.println(temp.data);
			temp=temp.next;
		}
	}
	public static void main(String args[]){
		Reverse r = new Reverse();
		r.insertEnd(1);
		r.insertEnd(2);
		r.insertEnd(3);
		r.insertEnd(4);
		r.printll();
		System.out.println("Reverse LinkedList is:");
		r.reverse();
		r.printll();
	}
}
