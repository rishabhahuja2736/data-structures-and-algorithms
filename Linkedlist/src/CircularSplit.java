//http://www.geeksforgeeks.org/sorted-insert-for-circular-linked-list/
public class CircularSplit {
	Node head, head1, head2, last;

	public class Node {
		int data;
		Node next;

		Node(int data) {
			this.data = data;
			next = null;
		}
	}

	public void circularEmpty(int data) {
		if (last != null) {
			return;
		}
		last = new Node(data);
		last.next = last;
	}

	public void circularEnd(int data) {
		Node temp = null;
		temp = new Node(data);
		temp.next = last.next;
		last.next = temp;
		last = temp;
	}

	public void traverse(Node temp) {
		Node first=temp;
		while (temp != null) {
			System.out.println(temp.data);
			temp = temp.next;
			if (temp == first) {
				return;
			}
		}
	}

	public void split() {
		head = last.next;
		Node slow_ptr = head;
		Node fast_ptr = head;
		while (fast_ptr.next != head && fast_ptr.next.next != head) {
			fast_ptr = fast_ptr.next.next;
			slow_ptr = slow_ptr.next;
		}
		if (fast_ptr.next.next == head) {
			fast_ptr = fast_ptr.next;
		}
		head1 = head;
		if (head.next != head)
			head2 = slow_ptr.next;
		slow_ptr.next = head1;
		fast_ptr.next = head2;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CircularSplit c = new CircularSplit();
		c.circularEmpty(1);
		c.circularEnd(2);
		c.circularEnd(3);
		c.circularEnd(4);
		c.head=c.last.next;
		System.out.println("Intial Elements");
		c.traverse(c.head);
	
		/* *** EVEN ELEMENTS *** */
		
		/*System.out.println("Splitting in case of even elements");
		c.split();
		System.out.println("First list");
		c.traverse(c.head1);
		System.out.println("Second List");
		c.traverse(c.head2);*/
		
		/* *** ODD ELEMENTS *** */
		
		System.out.println("Splitting in case of odd elements");
		c.circularEnd(5); 
		c.split();
		System.out.println("First list");
		c.traverse(c.head1);
		System.out.println("Second List");
		c.traverse(c.head2);
		
	}

}
