//http://www.geeksforgeeks.org/detect-and-remove-loop-in-a-linked-list/
public class LoopLL {
	Node head;
	public class Node {
		int data;
		Node next;

		Node(int data) {
			this.data = data;
			next = null;
		}
	}
	public void detectLoop(){
		Node slowptr=head;
		Node fastptr=head;
		while(slowptr!=null&&fastptr!=null){
			slowptr=slowptr.next;
			fastptr=fastptr.next.next;
			if(slowptr.data==fastptr.data){
				System.out.println("Loop is detected");
				break;
			}
		}
		slowptr=head;
		Node prev=null;
		while(slowptr!=null&&fastptr!=null){
			
			if(slowptr.data==fastptr.data){
				System.out.println(slowptr.data+" is the point where the loop starts");
				break;
			}
			prev=fastptr;
			slowptr=slowptr.next;
			fastptr=fastptr.next;
		}
		prev.next=null;
		/*System.out.println(prev.data);
		System.out.println(prev.next.data);
		System.out.println(prev.next.next.data);
		System.out.println(prev.next.next.next.data);
		System.out.println(prev.next.next.next.next.data);*/
	}
	public void createLoop(int data){
		Node temp=head;
		while(temp!=null&&temp.data!=data){
			temp=temp.next;
		}
		//System.out.println(head.next.next.next.next.data);
		head.next.next.next.next.next=temp;
		//System.out.println(head.next.next.next.next.data);
		//System.out.println(temp.data);
		
	}
	public void insertEnd(int data) {
		Node newnode = new Node(data);
		if (head == null) {
			head = newnode;
			return;
		}
		Node temp = head;
		while (temp.next != null) {
			temp = temp.next;
		}
		temp.next = newnode;
	}
	public void printll() {
		Node temp = head;
		while (temp != null) {
			System.out.println(temp.data);
			temp = temp.next;
		}
	}

	public static void main(String[] args) {
		LoopLL l = new LoopLL();
		l.insertEnd(1);
		l.insertEnd(2);
		l.insertEnd(3);
		l.insertEnd(4);
		l.insertEnd(5);
		l.printll();
		l.createLoop(2);
		l.detectLoop();
		System.out.println("After removing loop");
		l.printll();
	}	
}
