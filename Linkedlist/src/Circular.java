//http://www.geeksforgeeks.org/circular-singly-linked-list-insertion/
public class Circular {
	Node last;
	public class Node{
		int data;
		Node next;
		Node(int data){
			this.data=data;
			next=null;
		}
	}
	public void insertEmpty(int data){
		if(last!=null){
			return;
		}
		last = new Node(data);
		last.next=last;
	}
	public void insertBeg(int data){
		Node temp=new Node(data);
		temp.next=last.next;
		last.next=temp;
	}
	public void insertEnd(int data){
		Node temp=new Node(data);
		temp.next=last.next;
		last.next=temp;
		last=temp;
	}
	public void insertRandom(int data,int item){
		Node temp=new Node(data);
		Node loop=last.next;
		while(loop!=null){
			if(loop.next.data==item){
				break;
			}
			loop=loop.next;
		}
		temp.next=loop.next;
		loop.next=temp;
	}
	public void traverse(){
		Node temp=last.next;
		//System.out.println(temp.data);
		while(temp!=null){
			System.out.println(temp.data);
			temp=temp.next;
			if(temp==last.next){
				return;
			}
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated u stub
		Circular c = new Circular();
		c.insertEmpty(3);
		c.insertBeg(2);
		c.insertBeg(1);
		c.insertEnd(5);
		c.insertEnd(6);
		c.insertRandom(4,5);
		c.traverse();
	}

}
