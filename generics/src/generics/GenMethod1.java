package generics;


public class GenMethod1 {
	
	static <T> void Display(T[] arr){
		for(int i=0;i<arr.length;i++){
			if(arr[i]!=null)
			System.out.println(arr[i]);
			
		}
	}
	public static void main(String args[]){
		String[] names = new String[10];
		names[0]="rishabh";
		names[1]="ahuja";
		Display(names);
	}
}
