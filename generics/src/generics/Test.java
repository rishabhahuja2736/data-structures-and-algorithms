package generics;

public class Test {
	public static void main(String args[]){
		List<String> l = new List<String>(10);
		l.add("rishabh");
		l.add("ahuja");
		System.out.println("My name is:-"+l.toString());
		
		List<Integer> l2 = new List<Integer>(10);
		l2.add(1);
		l2.add(2);
		System.out.println("My name is:-"+l2.toString());
	}

}
