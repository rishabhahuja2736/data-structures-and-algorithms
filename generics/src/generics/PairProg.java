package generics;

public class PairProg {
	public static void main(String args[]){
		Pair<String,Integer> p1= new Pair<String,Integer>("Rishabh",1);
		Pair<String,Integer> p2= new Pair<String,Integer>("Ahuja",1);
		System.out.println(p1.first()+" "+p2.first());
	}

}
