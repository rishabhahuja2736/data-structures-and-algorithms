package generics;

public class NumberList<T> implements IList<T> {
	private T[] array;
	private int size;
	private int pos;

	public NumberList(int n){
		size=n;
		pos=0;
		array=(T[]) new Object[size];
	}
	@Override
	public void add(T element) {
		// TODO Auto-generated method stub 
		array[pos]=element;
		pos++;
	}

	@Override
	public T get(int n) {
		// TODO Auto-generated method stub
		return array[n] ;
	}
}
