package generics;

public class List<T>{
	private T[] array;
	private int size;
	private int pos;
	
	public List(int num){
		size=num;
		pos=0;
		array= (T[]) new Object[size];
	}
	public void add(T element){
		array[pos]=element;
		++pos;
	}
	public String toString(){
		String elements="";
		for(int i=0;i<pos;i++){
			elements +=array[i]+" ";
		}
		return elements;
	}
}
