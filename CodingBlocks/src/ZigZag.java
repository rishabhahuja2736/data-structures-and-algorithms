import java.util.Scanner;
import java.util.Stack;

public class ZigZag {
	class Node{
		int data;
		Node left,right;
		Node(int data){
			this.data=data;
			left=right=null;
		}
	}
	Node root;
	ZigZag(){
		Scanner sc = new Scanner(System.in);
		root=createTree(null,sc,false);
	}
	public Node createTree(Node node,Scanner sc,boolean isLeftRight){
		if(node==null){
			int data=sc.nextInt();
			node=new Node(data);
		}
		isLeftRight=sc.nextBoolean();
		if(isLeftRight){
			node.left=createTree(node.left,sc,false);
		}
		isLeftRight=sc.nextBoolean();
		if(isLeftRight){
			node.right=createTree(node.right,sc,false);
		}
		return node;
	}
	public void zigZag(Node node){
		Stack<Node> s1 = new Stack<Node>();
		Stack<Node> s2 = new Stack<Node>();
		if(node!=null){
			s1.push(node);
		}
		while(!s1.isEmpty()||!s2.isEmpty()){
			while(!s1.isEmpty()){
				Node temp=s1.pop();
				System.out.print(temp.data+" ");
				if(temp.left!=null){
					s2.push(temp.left);
				}
				if(temp.right!=null){
					s2.push(temp.right);
				}
			}
			while(!s2.isEmpty()){
				Node temp=s2.pop();
				System.out.print(temp.data+" ");
				if(temp.right!=null){
					s1.push(temp.right);
				}
				if(temp.left!=null){
					s1.push(temp.left);
				}
				
			}
		}
	}
	public static void main(String[] args) {
		ZigZag z = new ZigZag();
		z.zigZag(z.root);
	}
}