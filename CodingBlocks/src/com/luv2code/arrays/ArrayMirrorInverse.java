package com.luv2code.arrays;

import java.util.Scanner;

public class ArrayMirrorInverse {
	
	public void inverse(int[] arr1){
		int flag=1;
		for(int i=0;i<arr1.length;i++){
			if(arr1[arr1[i]]==i){
				flag=1;
			}else{
				flag=0;
				System.out.println("false");
				return;
			}
		}
		System.out.println("true");
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
	
		int n=sc.nextInt();
		int arr1[] = new int[n];
		for(int i=0;i<n;i++){
			arr1[i]=sc.nextInt();
		}
		new ArrayMirrorInverse().inverse(arr1);
	}

}
