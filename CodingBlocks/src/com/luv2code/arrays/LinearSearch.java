package com.luv2code.arrays;

import java.util.Scanner;

public class LinearSearch {
	public static int search(int arr[],int n){
		for(int i=0;i<arr.length;i++){
			if(arr[i]==n)
				return i;
		}
		return -1;
	}
	public static void main(String[] args) {
		int n,m;
		Scanner sc = new Scanner(System.in);
		n=sc.nextInt();
		int arr[] = new int[n];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		m=sc.nextInt();
		System.out.println(search(arr,m));
	}

}
