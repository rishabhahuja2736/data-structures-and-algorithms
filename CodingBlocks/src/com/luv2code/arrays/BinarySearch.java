package com.luv2code.arrays;

import java.util.Scanner;
public class BinarySearch {
	/*public int binarySearch(int arr[],int n,int start,int end){
		int s=0;
		int l=0;
		int mid=0;
		if(start>=0&&end>=0){
			s=start;
			l=end;
			mid=(l+s)/2;
		}	
		while(s<=l){
	        if(arr[mid]<n){
			mid=mid+1;
			binarySearch(arr,n,mid,l);
			}else if(arr[mid]>n){
				mid=mid-1;
				binarySearch(arr,n,s,mid);
			}else if(arr[mid]==n){
				return mid;
			}
		    
		}
		return -1;
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n=sc.nextInt();
		int arr[] = new int[n];
		for(int i=0;i<n;i++){
			arr[i]=sc.nextInt();
		}
		int m=sc.nextInt();
		System.out.println(new BinarySearch().binarySearch(arr,m,0,arr.length-1));
	}*/
	public static int binSear(int[] arr,int m){
        int left=0;
        int right=arr.length-1;
        int mid=0;
        while(left<=right){
            mid=(left+right)/2;
            if(arr[mid]==m){
                return mid;
            }else if(arr[mid]>m){
                right=mid-1;
            }else if(arr[mid]<m){
                left=mid+1;
            }
        }
        return -1;
    }
	public static void main(String args[]) {
	    Scanner s=new Scanner(System.in);
	    int N=s.nextInt();
	    int[] arr=new int[N];
	    for(int i=0;i<N;i++){
	        arr[i]=s.nextInt();
	    }
	    int M=s.nextInt();
	    System.out.println(binSear(arr,M));
	    }
	    
}
