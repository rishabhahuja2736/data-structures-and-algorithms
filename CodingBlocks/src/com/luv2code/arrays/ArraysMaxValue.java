package com.luv2code.arrays;

import java.util.Scanner;

public class ArraysMaxValue {
	public static void main(String[] args) {
		int n,max=0;
		Scanner sc = new Scanner(System.in);
		n=sc.nextInt();
		int arr[] = new int[n];
		for(int i=0;i<n;i++){
			arr[i]=sc.nextInt();
		}
		max=arr[0];
		for(int j=0;j<n-1;j++){
			if(arr[j+1]>max){
				max=arr[j+1];
			}
		}
		System.out.println(max);
	}
}
