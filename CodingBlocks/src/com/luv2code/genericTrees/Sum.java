package com.luv2code.genericTrees;

import java.util.ArrayList;
import java.util.Scanner;

public class Sum {
	class Node{
		int data;
		ArrayList<Node> children;
		Node(int data){
			this.data=data;
			children = new ArrayList<Node>(); 
		}
	}
	Node root;
	Sum(){
		Scanner sc = new Scanner(System.in);
		root=addNode(sc);
	}
	public Node addNode(Scanner sc){
		int data=sc.nextInt();
		Node newnode = new Node(data);
		int noofchildren=sc.nextInt();
		for(int i=0;i<noofchildren;i++){
			Node child=addNode(sc);
			newnode.children.add(child);
		}
		return newnode;
	}
	public int sum(Node node){
		int sum=0;
		if(node.children.isEmpty()){
			return node.data;
		}
		for(int i=0;i<node.children.size();i++){
			sum=sum+sum(node.children.get(i));
			//System.out.println(sum+" ");
		}
		return sum+node.data;
	}
	public static void main(String[] args) {
		Sum s = new Sum();
		System.out.println(s.sum(s.root));
	}

}
