package com.luv2code.genericTrees;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class GenericTree {
	class Node{
		int data;
		ArrayList<Node> children;
		Node(int data){
			this.data=data;
			children= new ArrayList<Node>();
		}
	}
	Node root;
	GenericTree(){
		Scanner sc = new Scanner(System.in);
		root=addNode(sc);
	}
	public Node addNode(Scanner sc){
		int data=sc.nextInt();
		Node newnode = new Node(data);
		
		int noofchildren= sc.nextInt();
		for(int i=1;i<=noofchildren;i++){
			Node child = addNode(sc);
			newnode.children.add(child);
		}
		return newnode;
	}
	public int max(Node node){
		if(node.children.isEmpty()){
			return node.data;
		}
		int max=node.data;
		for(int i=0;i<node.children.size();i++){
			int maxchild=max(node.children.get(i));
			if(maxchild>max){
				max=maxchild;
			}
		}
		return max;
	}
	public void levelOrder(){
		LinkedList<Node> queue= new LinkedList<Node>();
		queue.addLast(root);
		while(!queue.isEmpty()){
			Node node = queue.removeFirst();
			System.out.print(node.data+" ");
			for(int i=0;i<node.children.size();i++){
				queue.addLast(node.children.get(i));
			}
		}
	}
	public int height(Node node){
		int height=-1;
		for(int i=0;i<node.children.size();i++){
			int heightchild=height(node.children.get(i));
			if(heightchild>height){
				height=heightchild;
			}
		}
		return height;
	}
	public void display(Node node){
		System.out.print(node.data+" => ");
		for(int i=0;i<node.children.size();i++){
			System.out.print(node.children.get(i).data+", ");
		}
		System.out.println("END");
		for(int i=0;i<node.children.size();i++){
			display(node.children.get(i));
		}
	}
	public static void main(String[] args) {
		GenericTree g = new GenericTree();
		g.display(g.root);
		System.out.println("Max element is:-"+g.max(g.root));
		g.levelOrder();
	}
}
