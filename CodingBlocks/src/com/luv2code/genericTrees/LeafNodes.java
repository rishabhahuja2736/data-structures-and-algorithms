package com.luv2code.genericTrees;

import java.util.ArrayList;
import java.util.Scanner;

public class LeafNodes {
	class Node{
		int data;
		ArrayList<Node> children;
		Node(int data){
			this.data=data;
			children = new ArrayList<Node>();
		}
	}
	
	Node root;
	
	LeafNodes(){
		Scanner sc = new Scanner(System.in);
		root=addNode(sc);
	}
	
	public Node addNode(Scanner sc){
		int data=sc.nextInt();
		Node newnode = new Node(data);
		int noofchildren=sc.nextInt();
		for(int i=0;i<noofchildren;i++){
			Node child=addNode(sc);
			newnode.children.add(child);
		}
		return newnode;
	}
	public int countLeaf(Node node){
		int count=0;
		if(node.children.isEmpty()){
			return 1;
		}
		for(int i=0;i<node.children.size();i++){
			count=count+countLeaf(node.children.get(i));		}
		return count;
	}
	public static void main(String[] args) {
		LeafNodes l = new LeafNodes();
		System.out.println(l.countLeaf(l.root));
	}

}
