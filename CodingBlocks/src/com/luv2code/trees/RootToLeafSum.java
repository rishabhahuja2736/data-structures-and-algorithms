package com.luv2code.trees;

import java.util.Scanner;

public class RootToLeafSum {
	class Node{
		Node left,right;
		int data;
		Node(int data){
			this.data=data;
			left=right=null;
		}
	}
	Node root;
	RootToLeafSum(){
		Scanner sc = new Scanner(System.in);
		root=createTree(null,sc,false);
	}
	public Node createTree(Node node,Scanner sc,boolean isLeftRight){
		if(node==null){
			int data=sc.nextInt();
			node=new Node(data);
		}
		isLeftRight=sc.nextBoolean();
		if(isLeftRight){
			node.left=createTree(node.left,sc,false);
		}
		isLeftRight=sc.nextBoolean();
		if(isLeftRight){
			node.right=createTree(node.right,sc,false);
		}
		return node;
	}
	public boolean sum(Node node,int sum){
		if(node==null)
			return false;
		boolean ans=false;
		if(node!=null){
			int subsum=sum-node.data;
			if(node.left==null&&node.right==null&&subsum==0){
				return true;
			}
			ans=ans||sum(node.left,subsum);
			ans=ans||sum(node.right,subsum);
		}
		return ans;
	}
	public static void main(String[] args) {
		RootToLeafSum r = new RootToLeafSum();
		System.out.println(r.sum(r.root,60));
	}

}
