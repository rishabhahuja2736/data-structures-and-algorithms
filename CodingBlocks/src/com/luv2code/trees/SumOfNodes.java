package com.luv2code.trees;

import java.util.Scanner;

public class SumOfNodes {
	class Node{
		int data;
		Node left,right;
		Node(int data){
			left=right=null;
			this.data=data;
		}
	}
	Node root;
	
	SumOfNodes(){
		Scanner sc = new Scanner(System.in);
		root=createTree(null,sc,false);
	}
	public Node createTree(Node node,Scanner sc ,boolean isLeftRight){
		if(node==null){
			int data=sc.nextInt();
			node=new Node(data);
		}
		isLeftRight=sc.nextBoolean();
		if(isLeftRight){
			node.left=createTree(node.left,sc,false);
		}
		isLeftRight=sc.nextBoolean();
		if(isLeftRight){
			node.right=createTree(node.right,sc,false);
		}
		return node;
	}
	public int sum(Node node){
		if(node==null)
			return 0;
		if(node!=null){
			int a=sum(node.left);
			int b=sum(node.right);
			return node.data+a+b;
		}
		return 0;
	}
	public static void main(String[] args) {
		SumOfNodes s = new SumOfNodes();
		System.out.println(s.sum(s.root));
	}

}
