package com.luv2code.trees;

import java.util.Scanner;

public class ReplaceSum {
	class Node{
		Node left,right;
		int data;
		Node(int data){
			left=right=null;
			this.data=data;
		}
	}
	Node root;
	Node root2;
	public void addNode(Node node,int data){
		if(root==null){
			root= new Node(data);
		}else{
			Node newnode =  new Node(data);
			if(data<node.data){
				if(node.left!=null){
					addNode(node.left,data);
				}else{
					node.left=newnode;
				}
			}else if(data>node.data){
				if(node.right!=null){
					addNode(node.right,data);
				}else{
					node.right=newnode;
				}
			}
		}
	}
	public void display(Node node){
		if (node.left != null) {
			System.out.print(node.left.data + " => ");
		} else {
			System.out.print("END => ");
		}
		System.out.print(node.data);
		if (node.right != null) {
			System.out.print(" <= " + node.right.data);
		} else {
			System.out.print(" <= END");
		}
		System.out.println();

		if (node.left != null) {
			this.display(node.left);
		}

		if (node.right != null) {
			this.display(node.right);
		}
	}
	public int sum(Node node,int data){
		if(node==null){
			return 0;
		}
		{
			int left=sum(node.left,data);
			int right=sum(node.right,data);
			if(node.data>data&&node.data==root.data){
				return left+right+node.data;
			}
			if(node.data>data){
				return node.data+left+right;
			}
			return left+right;
		}
	}
	public void replace(Node node){
		if(node==null)
			return;
		if(node!=null){
			int replacedata=sum(root,node.data);
			Node newnode = new Node(replacedata);
			addNode2(root2,newnode);
			replace(node.left);
			replace(node.right);
		}
	}
	public void addNode2(Node node,Node newnode){
		if(root2==null){
			root2=newnode;
		}else{
			if(newnode.data>node.data){
				if(node.left!=null){
					addNode2(node.left,newnode);
				}else{
					node.left=newnode;
				}
			}else if(newnode.data<node.data){
				if(node.right!=null){
					addNode2(node.right,newnode);
				}else{
					node.right=newnode;
				}
			}
		}
	}
	
	public static void main(String[] args) {
		ReplaceSum r = new ReplaceSum();
		Scanner sc = new Scanner(System.in);
		int n =sc.nextInt();
		for(int i=1;i<=n;i++){
			int data=sc.nextInt();
			r.addNode(r.root,data);
		}
		r.replace(r.root);
		r.display(r.root2);
	}

}
