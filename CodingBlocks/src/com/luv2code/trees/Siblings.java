package com.luv2code.trees;
import java.util.Scanner;

public class Siblings {
	public class Node{
		int data;
		Node left,right;
		Node(int data){
			this.data=data;
			left=right=null;
		}
	}
	Node root;
	Siblings(){
		Scanner sc = new Scanner(System.in);
		root=createTree(null,sc,false);
	}
	public Node createTree(Node node,Scanner sc,boolean isLeftRight){
		if(node==null){
			int data=sc.nextInt();
			node=new Node(data);
		}
		isLeftRight=sc.nextBoolean();
		if(isLeftRight){
			node.left=createTree(node.left,sc,false);
		}
		isLeftRight=sc.nextBoolean();
		if(isLeftRight){
			node.right=createTree(node.right,sc,false);
		}
		return node;	
	}
	
	public void sum(Node node){
		if(node==null){
			return ;
		}
		if(node!=null){
			if(node.left==null&&node.right!=null){
				System.out.print(node.right.data+" ");
			}
			if(node.left!=null&&node.right==null){
				System.out.println(node.left.data+" ");
			}
			sum(node.left);
			sum(node.right);
		}
	}
	public static void main(String[] args) {
		Siblings r = new Siblings();
		r.sum(r.root);
	}
}
