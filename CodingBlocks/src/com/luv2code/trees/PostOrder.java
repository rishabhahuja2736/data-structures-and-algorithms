package com.luv2code.trees;

import java.util.Scanner;

public class PostOrder {
	class Node{
		Node left,right;
		int data;
		Node(int data){
			this.data=data;
			left=right=null;
		}
	}
	class Index 
	{
		int postindex;
	}
	Node root;
	public Node createTree(int arr1[],int arr2[],int start,int end,Index index){
		if(start>end){
			return null;
		}
		Node node = new Node(arr1[index.postindex]);
		(index.postindex)--;
		if(start==end)
			return node;
		int inindex=search(node.data,arr2,start,end);
		node.right=createTree(arr1,arr2,inindex+1,end,index);
		node.left=createTree(arr1,arr2,start,inindex-1,index);
		return node;
	}
	public int search(int ele,int arr[],int start,int end){
		for(int i=start;i<=end;i++){
			if(arr[i]==ele)
				return i;
		}
		return -1;
	}
	public Node buildTree(int arr1[],int arr2[],int n){
		Index i = new Index();
		i.postindex=n-1;
		return createTree(arr1, arr2, 0, arr1.length-1,i);
	}
	public void preorder(Node root){
		if(root!=null)
		{
			System.out.println(root.data);
			preorder(root.left);
			preorder(root.right);
		}
	}
	private void display(Node node) {
		if (node == null) {
			return;
		}

		String str = "";

		if (node.left != null) {
			str += node.left.data;
		} else {
			str += "END";
		}

		str += " => " + node.data + " <= ";

		if (node.right != null) {
			str += node.right.data;
		} else {
			str += "END";
		}

		System.out.println(str);

		this.display(node.left);
		this.display(node.right);
	}
	public static void main(String args[]){
		Scanner sc = new Scanner(System.in);
		int n=sc.nextInt();
		int arr1[] = new int[n];
		for(int i=0;i<n;i++){
			arr1[i]=sc.nextInt();
		}
		int m=sc.nextInt();
		int arr2[] = new int[m];
		for(int i=0;i<m;i++){
			arr2[i]=sc.nextInt();
		}
		PostOrder p = new PostOrder();
		//p.index(arr1);
		int postindex=arr1.length;
		p.root=p.buildTree(arr1,arr2,postindex);;
		p.preorder(p.root);
	}
}
