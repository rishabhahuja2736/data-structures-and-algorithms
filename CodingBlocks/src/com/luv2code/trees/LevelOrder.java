package com.luv2code.trees;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class LevelOrder {
	class Node {
		int data;
		Node left,right;
		Node(int data){
			left=right=null;
			this.data=data;
		}
	}
	Node root;
	LevelOrder(){
		Scanner sc = new Scanner(System.in);
		root=createTree(null,sc,false);
	}
	public Node createTree(Node parent,Scanner sc,boolean isleftright){
		if(parent==null){
			int data=sc.nextInt();
			parent=new Node(data);
		}
		isleftright=sc.nextBoolean();
		if(isleftright){
			parent.left=createTree(parent.left,sc,false);
		}
		isleftright=sc.nextBoolean();
		if(isleftright){
			parent.right=createTree(parent.right,sc,false);
		}
		return parent;
	}
	public void level(Node root){
		Queue<Node> q1 = new LinkedList<Node>();
		Queue<Node> q2 = new LinkedList<Node>();
		if(root!=null){
			q1.add(root);
		}
		while(!q1.isEmpty()||!q2.isEmpty()){
			while(!q1.isEmpty()){
				Node temp=q1.poll();
				System.out.print(temp.data+" ");
				if(temp.left!=null)
					q2.add(temp.left);
				if(temp.right!=null)
					q2.add(temp.right);
			}
			System.out.println();
			while(!q2.isEmpty()){
				Node temp=q2.poll();
				System.out.print(temp.data+" ");
				if(temp.left!=null)
					q1.add(temp.left);
				if(temp.right!=null)
					q1.add(temp.right);
			}
			System.out.println();
		}
	}
	public void traversal(Node node){
		if(node!=null){
			System.out.println(node.data);
			traversal(node.left);
			traversal(node.right);
		}
	}
	public static void main(String[] args) {
		LevelOrder o =new LevelOrder();
		o.level(o.root);
	}

}
