
package com.luv2code.trees;

import java.util.Scanner;

public class RootToLeafPaths {
	public class Node{
		Node left,right;
		int data;
		Node(int data){
			this.data=data;
			left=right=null;
		}
	}
	Node root;
	RootToLeafPaths(){
		Scanner sc = new Scanner(System.in);
		root=createTree(null,sc,false);
	}
	public Node createTree(Node node,Scanner sc,boolean isLeftRight){
		if(node==null){
			int data=sc.nextInt();
			node= new Node(data);
		}
		isLeftRight=sc.nextBoolean();
		if(isLeftRight){
			node.left=createTree(node.left,sc,false);
		}
		isLeftRight=sc.nextBoolean();
		if(isLeftRight){
			node.right=createTree(node.right,sc,false);
		}
		return node;
	}
	public void path(Node node,int arr[],int i){
		if(node==null)
			return;
		if(node!=null){
			arr[i++]=node.data;
			if(node.left==null&&node.right==null){
					for(int j=0;j<(i);j++){
						System.out.print(arr[j]+" ");
					}
					System.out.println();
			}
			path(node.left,arr,i);
			path(node.right,arr,i);
		}
	}
	public static void main(String[] args) {
		RootToLeafPaths r = new RootToLeafPaths();
		int arr[] = new int[1000];
		r.path(r.root, arr,0);
	}

}
