package com.luv2code.trees;

import java.util.Scanner;

public class Identical {
	class Node{
		Node left,right;
		int data;
		Node(int data){
			this.data=data;
			left=right=null;
		}
	}
	static Node root1;
	static Node root2;
	Identical(){
		Scanner sc = new Scanner(System.in);
		root1=createTree(null,sc,false);
		root2=createTree(null,sc,false);
	}
	public  Node createTree(Node node,Scanner sc,boolean isLeftRight){
		if(node==null){
			int data=sc.nextInt();
			node=new Node(data);
		}
		isLeftRight=sc.nextBoolean();
		if(isLeftRight){
			node.left=createTree(node.left,sc,false);
		}
		isLeftRight=sc.nextBoolean();
		if(isLeftRight){
			node.right=createTree(node.right,sc,false);
		}
		return node;
	}
	public boolean identical(Node root1,Node root2){
		if(root1==null&&root2==null)
			return true;
		if(root1.data==root2.data){
			boolean left=identical(root1.left,root2.left);
			boolean right=identical(root1.right,root2.right);
			return left&&right;
		}
		return false;
	}
	public void preorder(Node root){
		if(root!=null){
			System.out.print(root.data+" ");
			preorder(root.left);
			preorder(root.right);
		}
	}
	public static void main(String[] args) {
		Identical i1 = new Identical();
		//Identical i2 = new Identical(root1);
		/*i1.preorder(i1.root1);
		System.out.println();
		i1.preorder(i1.root2);*/
		System.out.println(i1.identical(root1, root2));
	}

}
