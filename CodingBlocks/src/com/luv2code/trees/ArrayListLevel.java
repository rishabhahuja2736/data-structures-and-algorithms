package com.luv2code.trees;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class ArrayListLevel {
	class Node {
		int data;
		Node left, right;

		Node(int data) {
			left = right = null;
			this.data = data;
		}
	}

	Node root;

	ArrayListLevel() {
		Scanner sc = new Scanner(System.in);
		root = createTree(null, sc, false);
	}

	public Node createTree(Node parent, Scanner sc, boolean isleftright) {
		if (parent == null) {
			int data = sc.nextInt();
			parent = new Node(data);
		}
		isleftright = sc.nextBoolean();
		if (isleftright) {
			parent.left = createTree(parent.left, sc, false);
		}
		isleftright = sc.nextBoolean();
		if (isleftright) {
			parent.right = createTree(parent.right, sc, false);
		}
		return parent;
	}

	public ArrayList<ArrayList<Integer>> level(Node root) {
		Queue<Node> q1 = new LinkedList<Node>();
		Queue<Node> q2 = new LinkedList<Node>();
		ArrayList<ArrayList<Integer>> al = new ArrayList<ArrayList<Integer>>();
		if (root != null) {
			q1.add(root);
		}
		while (!q1.isEmpty() || !q2.isEmpty()) {
			ArrayList<Integer> al2 = new ArrayList<Integer>();
			while (!q1.isEmpty()) {
				Node temp = q1.poll();
				al2.add(temp.data);
				if (temp.left != null)
					q2.add(temp.left);
				if (temp.right != null)
					q2.add(temp.right);
			}
			if (!al2.isEmpty())
				al.add(al2);
			ArrayList<Integer> al3 = new ArrayList<Integer>();
			while (!q2.isEmpty()) {
				Node temp = q2.poll();
				al3.add(temp.data);
				if (temp.left != null)
					q1.add(temp.left);
				if (temp.right != null)
					q1.add(temp.right);
			}
			if (!al3.isEmpty())
				al.add(al3);
		}
		System.out.println(al);
		return al;
	}

	public void traversal(Node node) {
		if (node != null) {
			System.out.println(node.data);
			traversal(node.left);
			traversal(node.right);
		}
	}

	public static void main(String[] args) {
		ArrayListLevel l = new ArrayListLevel();
		l.level(l.root);

	}
}
