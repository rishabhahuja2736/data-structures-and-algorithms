
package com.luv2code.trees;

import java.util.Scanner;

public class RootLeafSumPath {
	public class Node{
		Node left,right;
		int data;
		Node(int data){
			this.data=data;
			left=right=null;
		}
	}
	Node root;
	RootLeafSumPath(){
		Scanner sc = new Scanner(System.in);
		root=createTree(null,sc,false);
		int sum= sc.nextInt();
		int arr[] = new int[100];
		path(root, arr,0, sum);
	}
	public Node createTree(Node node,Scanner sc,boolean isLeftRight){
		if(node==null){
			int data=sc.nextInt();
			node= new Node(data);
		}
		isLeftRight=sc.nextBoolean();
		if(isLeftRight){
			node.left=createTree(node.left,sc,false);
		}
		isLeftRight=sc.nextBoolean();
		if(isLeftRight){
			node.right=createTree(node.right,sc,false);
		}
		return node;
	}
	public void path(Node node,int arr[],int i,int sum){
		if(node==null)
			return;
		if(node!=null){
			sum=sum-node.data;
			arr[i++]=node.data;
			if(node.left==null&&node.right==null){
				if(sum==0){
					for(int j=0;j<(i);j++){
						System.out.print(arr[j]+" ");
					}
					System.out.println();
				}
			}
			path(node.left,arr,i,sum);
			path(node.right,arr,i,sum);
		}
	}
	public static void main(String[] args) {
		RootLeafSumPath r = new RootLeafSumPath();
		//int arr[] = new int[100];
		//Scanner sc = new Scanner(System.in);
		
		//r.path(r.root, arr,0, 60);
	}

}
