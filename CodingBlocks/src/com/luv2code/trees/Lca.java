package com.luv2code.trees;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Lca {
	class Node{
		Node left,right;
		int data;
		Node(int data){
			left=right=null;
			this.data=data;
		}
	}
	Node root;
	Lca(){
		Scanner sc = new Scanner(System.in);
		root=createTree(root,sc,false);
	}
	public Node createTree(Node parent,Scanner sc,boolean isLeftRight){
		if(parent==null){
			int data=sc.nextInt();
			parent=new Node(data);
		}
		isLeftRight=sc.nextBoolean();
		if(isLeftRight){
			parent.left=createTree(parent.left,sc,false);
		}
		isLeftRight=sc.nextBoolean();
		if(isLeftRight){
			parent.right=createTree(parent.right,sc,false);
		}
		return parent;
	}
	public Node lca(Node root,int a,int b){	
		if(root==null)
			return null;
		if(root.data==a||root.data==b){
			return root;
		}
		Node left=lca(root.left,a,b);
		Node right=lca(root.right,a,b);
		if(left!=null&&right!=null)
			return root;
		else if(left!=null){
		    return left;
		}else{
		    return right;
		}
		//return left!=null?left:right;
	}
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		Lca l = new Lca();
		int a=sc.nextInt();
		int b=sc.nextInt();
		System.out.print(l.lca(l.root,a,b).data);
	}

}