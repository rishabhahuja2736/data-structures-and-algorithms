package com.luv2code.basics;
import java.util.*;
public class PrintPrime {
	public static void main(String[] args) {
		int n;
		Scanner sc = new Scanner(System.in);
		n=sc.nextInt();
		for(int i=2;i<=n;i++){
			innerLoop:
			{
				for(int j=2;j<i;j++){
					if(i%j==0)
						break innerLoop;
				}
				System.out.println(i);
			}
				
		}
	}
}
