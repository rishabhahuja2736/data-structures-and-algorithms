package com.luv2code.basics;
import java.util.*;
public class PascalTriangle {
	public static void main(String args[]){
		int n,count=2;
		Scanner sc = new Scanner(System.in);
		n=sc.nextInt();
		for(int i=0;i<n;i++){
			for(int j=i;j<=i;j++){
				if(j==1||j==i){
					System.out.print(1);
				}else{
					System.out.print(count);
				}
			}
			count++;
		}
	}
}
