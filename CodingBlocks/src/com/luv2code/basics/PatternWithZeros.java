package com.luv2code.basics;
import java.util.*;
public class PatternWithZeros {
	public static void main(String[] args) {
		int n,count=1;
		Scanner sc = new Scanner(System.in);
		n=sc.nextInt();
		for(int i=1;i<=n;i++){
			for(int j=1;j<=i;j++){
				if(i==1||i==2)
					System.out.print(count+"\t");
				else if(i>2&&(j==1||j==i))
					System.out.print(count+"\t");
				else
					System.out.print(0+"\t");
			}
			count++;
			System.out.println();
		}
	}
}
