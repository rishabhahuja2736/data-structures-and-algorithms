package com.luv2code.basics;

import java.util.Scanner;

public class PatternTriangle {
	public static void main(String[] args) {
		int n, count = 0;
		Scanner sc = new Scanner(System.in);
		n = sc.nextInt();
		for (int i = n; i > 0; i--) {
			for (int j = i - 1; j > 0; j--) {
				System.out.print("\t");
			}
			for (int k = 0; k < (2 * (n - i) + 1); k++) {
				if (k <= (n - i))
					System.out.print((++count) + "\t");
				else
					System.out.print((--count) + "\t");
			}
			System.out.println();
		}
	}

}
