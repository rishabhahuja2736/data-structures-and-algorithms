package com.luv2code.basics;
import java.util.*;
public class PrintFibonacciNumbers {
	public static void main(String[] args) {
		int n,a=0,b=1,c;
		Scanner sc = new Scanner(System.in);
		n=sc.nextInt();
		while(a<=n){
			System.out.println(a);
			c=a+b;
			a=b;
			b=c;
		}
	}

}

