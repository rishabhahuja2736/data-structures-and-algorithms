package com.luv2code.basics;

import java.util.Scanner;

public class CharacterType {
	public static void main(String args[]){
		Character ch;
		Scanner sc = new Scanner(System.in);
		ch=sc.next().charAt(0);
		if(ch.isUpperCase(ch)){
			System.out.println('U');
		}else if(ch.isLowerCase(ch)){
			System.out.println('L');
		}else{
			System.out.println('I');
		}
		
	}
}
