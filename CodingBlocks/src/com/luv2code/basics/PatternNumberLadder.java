package com.luv2code.basics;
import java.util.*;
public class PatternNumberLadder {
	public static void main(String args[]){
		int n,count=1;
		Scanner sc = new Scanner(System.in);
		n=sc.nextInt();
		for(int i=1;i<=n;i++){
			for(int j=1;j<=i;j++)
			{
				System.out.print((count++)+"\t");
			}
			System.out.println("");
		}
	}
}
