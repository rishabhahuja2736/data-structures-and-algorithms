package com.luv2code.basics;

import java.util.Scanner;

public class IsArmstrongNumber {
	static int power(int a,int b){
		int sum=1;
		while(b>0){
			sum*=a;
			b--;
		}
		return sum;
	}
	static int order(int num){
		int count=0;
		while(num>0){
			num=num/10;
			count++;
		}
		return count;
	}
	public static void main(String args[]){
		Scanner sc = new Scanner(System.in);
		int num1=sc.nextInt();
		int num=num1;
		int rem=0,sum=0;
		int order=order(num);
		while(num>0){
			rem=num%10;
			sum=sum+power(rem,order);
			num=num/10;
		}
		if(sum==num1){
			System.out.println("true");
		}else{
			System.out.println("false");
		}
	}
}
