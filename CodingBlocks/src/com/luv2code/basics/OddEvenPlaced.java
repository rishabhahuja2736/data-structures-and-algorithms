package com.luv2code.basics;

import java.util.Scanner;

public class OddEvenPlaced {
	public static void main(String args[]){
		int n,sum1=0,sum2=0,flag=0;
		Scanner sc = new Scanner(System.in);
		n=sc.nextInt();
		
		while(n!=0){
			if(flag==0){
				sum1=sum1+n%10;
				n=n/10;
				flag=1;
			}
			else if(flag==1){
				sum2=sum2+n%10;
				n=n/10;
				flag=0;
			}
		}
		System.out.println(sum1);
		System.out.println(sum2);
	}
}
