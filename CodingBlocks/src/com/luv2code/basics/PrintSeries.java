package com.luv2code.basics;
import java.util.Scanner;
public class PrintSeries {
	public static void main(String args[]){
		Scanner sc = new Scanner(System.in);
		int n1,n2,sum,count=0,i=0;
		n1=sc.nextInt();
		n2=sc.nextInt();
		while(count<n1){
			sum=3*(++i)+2;
			if(sum%n2!=0){
				System.out.println(sum);
				count++;
			}
		}
	}
}
