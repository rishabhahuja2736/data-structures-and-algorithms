package com.luv2code.basics;
import java.util.*;
public class FibonacciPattern {
	public static void main(String[] args) {
		int n,a=0,b=1,c;
		Scanner sc = new Scanner(System.in);
		n=sc.nextInt();
		for(int i=1;i<=n;i++){
			for(int j=1;j<=i;j++){
				System.out.print(a+"\t");
				c=a+b;
				a=b;
				b=c;
			}
			System.out.println();
		}
	}

}
