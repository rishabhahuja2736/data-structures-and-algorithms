package com.luv2code.basics;

import java.util.Scanner;

public class PatternRhombus {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n,count=1;
		Scanner sc = new Scanner(System.in);
		n=sc.nextInt();
		for(int i=0;i<(n/2)+1;i++){
			for(int j=(n/2);j>i;j--){
				System.out.print("\t");
			}
			for(int k=0;k<(2*i+1);k++){
				System.out.print("*"+"\t");
			}
			System.out.println();
		}
		for(int i=(n/2)-1;i>=0;i--){
			for(int j=0;j<count;j++){
				System.out.print("\t");
			}
			count++;
			for(int k=0;k<(2*i+1);k++){
				System.out.print("*"+"\t");
			}
			System.out.println();
		}
	}

}
