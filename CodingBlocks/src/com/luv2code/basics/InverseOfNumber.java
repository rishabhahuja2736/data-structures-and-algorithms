package com.luv2code.basics;

import java.util.Scanner;

public class InverseOfNumber {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num=sc.nextInt();
		int rem,count=1,temp,sum=0;
		while(num>0){
			rem=num%10;
			temp=count;
			while((rem-1)>0){
				temp*=10;
				rem--;
			}
			sum=sum+temp;
			count++;
			num=num/10;
		}
		System.out.println(sum);
	}
}
