package com.luv2code.basics;

import java.util.Scanner;

public class DecimalToOctal {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num=sc.nextInt();
		int rem,sum=0,count=0;
		while(num>0){
			rem=num%8;
			sum=(int) (sum+rem*Math.pow(10, count));
			count++;
			num=num/8;
		}
		System.out.println(sum);
	}
}
