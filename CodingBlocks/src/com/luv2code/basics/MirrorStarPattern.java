package com.luv2code.basics;
import java.util.*;
public class MirrorStarPattern {
	public static void main(String args[]){
		int n,count=1,count1=0,k=0;
		Scanner sc = new Scanner(System.in);
		n=sc.nextInt();
		for(int i=n;i>0;i--){
			for(int j=i-1;j>0;j--){
				System.out.print("\t");
			}
			for(k=0;k<(2*(n-i)+1);k++){
				if(k<=n-i)
					System.out.print(++count1+"\t");
				else
					System.out.print(--count1+"\t");
			}
			System.out.println();
		}
		count1=count1-2;
		for(int i=n-1;i>0;i--){
			for(int j=0;j<count;j++){
				System.out.print("\t");
			}
			count++;
			for(int l=0;l<2*(i-1)+1;l++){
					if(l<=i-1)
						System.out.print(++count1+"\t");
					else
						System.out.print(--count1+"\t");
			}
			count1=count1-2;
			System.out.println();
		}
	}	
}
