package com.luv2code.basics;

import java.util.Scanner;
public class BasicCalculator {
	public static void operation(){
		Scanner sc = new Scanner(System.in);
		int n1=0,n2=0;
		char ch;
		do{
			ch=sc.next().charAt(0);
			if(ch=='+'||ch=='-'||ch=='*'||ch=='%'||ch=='/'){
				n1=sc.nextInt();
				n2=sc.nextInt();
			}
			if(ch=='+'){
				System.out.println(n1+n2);
			}else if(ch=='-'){
				System.out.println(n1-n2);
			}else if(ch=='*'){
				System.out.println(n1*n2);
			}else if(ch=='/'){
				System.out.println(n1/n2);
			}else if(ch=='%'){
				System.out.println(n1%n2);
			}else if(ch=='X'||ch=='x'){
				return;
			}else{
				System.out.println("Invalid operation. Try again.");
			}
		}while(true);
	}
	public static void main(String args[]){
		operation();
	}
}

/*public static void operation(char ch,int n1,int n2){
switch(ch){
	case '+':
		System.out.println(n1+n2);
		break;
	case '-':
		System.out.println(n1-n2);
		break;
	case '*':
		System.out.println(n1*n2);
		break;
	case '/':
		System.out.println(n1/n2);
		break;
	case '%':
		System.out.println(n1%n2);
		break;
	case 'X':
		return;
	case 'x':
		return;		
	default:
		System.out.println("Invalid operation. Try again.");
		
}	
}*/