package com.luv2code.Strings;

import java.util.Scanner;

public class CountPalidromicSubstring {
	public static boolean palidrome(String s){
		int l=0;
		int r=s.length()-1;
		while(l<=r){
			if(s.charAt(l)==s.charAt(r)){
				l++;
				r--;
			}else{
				return false;
			}
		}
		return true;
	}
	public static void palidromeString(String s){
		int count=0,temp=1;
		for(int i=0;i<=s.length()-1;i++){
			for(int j=temp;j<=s.length();j++){
				//System.out.println(s.substring(i, j));
				if(palidrome(s.substring(i, j))){
					count++;
				}
			}
			temp++;
		}
		System.out.print(count);
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String s=sc.next();
		palidromeString(s);
	}
}
