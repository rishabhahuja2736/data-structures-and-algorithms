package com.luv2code.Strings;

import java.util.Scanner;

public class IsPalindrome {
	public static void palindrome(String s){
		int l=0;
		int r=s.length()-1;
		while(l<=r){
			if(s.charAt(l)!=s.charAt(r)){
				System.out.println("false");
				return;
			}
			l++;
			r--;
		}
		System.out.println("true");
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String s=sc.next();
		palindrome(s);
	}

}
