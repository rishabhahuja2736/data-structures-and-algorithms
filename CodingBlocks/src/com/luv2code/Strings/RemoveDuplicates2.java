package com.luv2code.Strings;

import java.util.Scanner;

public class RemoveDuplicates2 {
	public static void removeDuplicates(String s){
		String ans="";
		for(int i=0;i<s.length()-1;i++){
			char ch1=s.charAt(i);
			char ch2=s.charAt(i+1);		
			if(ch1!=ch2){
				ans+=ch1;
			}
		}
		ans+=s.charAt(s.length()-1);
		System.out.println(ans);
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String s1= sc.next();
		removeDuplicates(s1);		
	}
}
/*if(s.charAt(s.length()-1)!=s.charAt(s.length()-2)){
ans+=s.charAt(s.length()-1);
System.out.println(ans);
}else{
System.out.println(ans);
}*/