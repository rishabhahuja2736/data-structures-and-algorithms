package com.luv2code.recursion;

import java.util.Arrays;
import java.util.Scanner;

//Java program to print all permutations of a
//given string.
public class Permutation
{	
	public static void permute(String str,int l,int h){
		if(l==h)
			System.out.print(str+" ");
		else{
			for(int i=l;i<=h;i++){
				str=swap(str,l,i);
				permute(str,l+1,h);
				str=swap(str,l,i);
			}
		}
	}
	public static String swap(String str ,int i , int j){
		char arr[]=str.toCharArray();
		char temp=arr[i];
		arr[i]=arr[j];
		arr[j]=temp;
		return String.valueOf(arr);
	}
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		String str=sc.next();
		int n = str.length();
		permute(str,0,n-1);
	}
}