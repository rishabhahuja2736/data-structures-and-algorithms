package com.luv2code.recursion;

import java.util.Scanner;

public class CountRemoveReplace {
	public static void crr1(String s,int i,int count){
		if(i>=s.length()-2){
			if(s.charAt(s.length()-1)=='i'&&s.charAt(s.length()-2)=='h')
				count++;
			System.out.println(count);
			return;
		}
		if(s.charAt(i)=='h'&&s.charAt(i+1)=='i'&&s.charAt(i+2)!='t')
			count++;
		crr1(s,i+1,count);
	}
	public static void crr2(String s,int i){
		StringBuilder sb= new StringBuilder(s);
		if(i==s.length()-2){
			if(s.charAt(s.length()-1)=='i'&&s.charAt(s.length()-2)=='h'){
				/*sb.deleteCharAt(sb.length()-1);
				sb.deleteCharAt(sb.length()-1);*/
				sb.delete(s.length()-2,s.length());
			}
			System.out.println(sb.toString());
			return;
		}
		if(s.charAt(i)=='h'&&s.charAt(i+1)=='i'&&s.charAt(i+2)!='t'){
			/*sb.deleteCharAt(i);
			sb.deleteCharAt(i);*/
			sb.delete(i,i+2);
			crr2(sb.toString(),0);
			return;
		}
		crr2(sb.toString(),i+1);
	}
	public static void crr3(String s,int i){
		StringBuilder sb= new StringBuilder(s);
		if(i>=s.length()-2){
			if(s.charAt(s.length()-1)=='i'&&s.charAt(s.length()-2)=='h'){
				//sb.delete(s.length()-2,s.length());
				sb.replace(s.length()-2,s.length(),"bye");
			}
			System.out.println(sb.toString());
			return;
		}
		if(s.charAt(i)=='h'&&s.charAt(i+1)=='i'&&s.charAt(i+2)!='t'){
			//sb.delete(i,i+2);
			sb.replace(i,i+2,"bye");
		}
		crr3(sb.toString(),i+1);
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String s = sc.next();
		crr1(s,0,0);
		crr2(s,0);
		crr3(s,0);
	}

}
