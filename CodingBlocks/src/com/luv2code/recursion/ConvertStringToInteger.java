package com.luv2code.recursion;

import java.util.Scanner;

public class ConvertStringToInteger {
	public static void convert(String s,int i){
		if(i>=s.length()){
			return;
		}
		System.out.print(Character.getNumericValue(s.charAt(i)));
		convert(s,i+1);
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String s= sc.next();
		convert(s,0);
	}

}
