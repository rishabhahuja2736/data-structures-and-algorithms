package com.luv2code.recursion;

import java.util.Scanner;

public class Palindrome {
	public static void palindrome(int arr[],int j,int k){
		if(j>k){
			System.out.println("true");
			return;
		}
		if(arr[j]!=arr[k]){
			System.out.println("false");
			return;
		}
		palindrome(arr,j+1,k-1);
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n=sc.nextInt();
		int arr[] = new int[n];
		for(int i=0;i<n;i++){
			arr[i]=sc.nextInt();
		}
		palindrome(arr,0,arr.length-1);
		/*int j=0,k=arr.length-1;
		while(j<=k){
			if(arr[j++]!=arr[k--]){
				System.out.println("false");
				return;
			}
		}
		System.out.println("true");*/
	}
}
