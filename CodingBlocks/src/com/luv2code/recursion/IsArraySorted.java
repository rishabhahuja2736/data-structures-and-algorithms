package com.luv2code.recursion;

import java.util.Scanner;

public class IsArraySorted {
	public static void isArraySorted(int arr[],int n,int i){
		if(i>=n-1)
		{
			System.out.println("true");
			return;
		}
		if(arr[i]>arr[i+1]){
			System.out.println("false");
			return;
		}
		isArraySorted(arr,n,i+1);
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n=sc.nextInt();
		int arr[] = new int[n];
		
		for(int i=0;i<n;i++){
			arr[i]=sc.nextInt();
		}
		isArraySorted(arr,n,0);
		/*
		for(int i=0;i<n-1;i++){
			if(arr[i]>arr[i+1]){
				System.out.println("false");
				return;
			}
		}
		System.out.println("true");*/
	}

}
