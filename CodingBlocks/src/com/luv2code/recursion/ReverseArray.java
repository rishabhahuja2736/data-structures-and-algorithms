package com.luv2code.recursion;

import java.util.Scanner;

public class ReverseArray {
	public static void reverse(int arr1[],int arr2[],int n,int i,int j){
		if(i<0){
			return;
		}
		arr2[j]=arr1[i];
		System.out.print(arr2[j]+" ");
		reverse(arr1,arr2,n,i-1,j+1);
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n=sc.nextInt();
		int arr1[] = new int[n];
		int arr2[] = new int[n];
		for(int i=0;i<n;i++){
			arr1[i]=sc.nextInt();
		}
		reverse(arr1,arr2,n,arr1.length-1,0);
	}

}
