package com.luv2code.recursion;

import java.util.Scanner;

public class ContainedInAnArray {
	public static void containedArray(int arr[],int n,int i,int m){
		if(i>=n){
			System.out.println("false");
			return;
		}
		if(arr[i]==m){
			System.out.println("true");
			return;
		}
		containedArray(arr,n,i+1,m);
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n=sc.nextInt();
		int arr[] = new int[n];
		for(int i=0;i<n;i++){
			arr[i]=sc.nextInt();
		}
		int m =sc.nextInt();
		containedArray(arr,n,0,m);
		/*for(int i=0;i<n;i++){
			if(arr[i]==m){
				System.out.println("true");
				return;
			}
		}
		System.out.println("false");*/
	}

}
