package com.luv2code.recursion;

import java.util.Scanner;

public class Palindrome2 {
	public static void checkPalindrome(String s1,int i,int j){
		if(i>=s1.length()){
			System.out.println("true");
			return;
		}
		if(s1.charAt(i)!=s1.charAt(j))
		{
			System.out.println("false");
			return;
		}
		checkPalindrome(s1,i+1,j-1);
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String s1=sc.next();
		checkPalindrome(s1,0,s1.length()-1);
	}
}
