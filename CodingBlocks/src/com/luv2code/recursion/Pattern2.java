package com.luv2code.recursion;

import java.util.Scanner;

public class Pattern2 {
	public static void pattern(int i,int j,int n){
		if(i<1)
			return;
		if(j>i){
			System.out.println();
			pattern(i-1,1,n);
			return;
		}
		System.out.print("*"+"\t");
		pattern(i,j+1,n);
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n=sc.nextInt();
		pattern(n,1,n);
		/*for(int i=n;i>=1;i--){
			for(int j=1;j<=i;j++){
				System.out.print("*"+"\t");
			}
			System.out.println();
		}*/
	}

}
