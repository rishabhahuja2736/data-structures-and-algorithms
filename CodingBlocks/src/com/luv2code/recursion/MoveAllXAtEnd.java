package com.luv2code.recursion;

import java.util.Scanner;

public class MoveAllXAtEnd {
	public static void moveX(String s,int i){
		StringBuilder sb = new StringBuilder(s);
		if(i>=s.length()){
			System.out.println(s);
			return;
		}
		if(s.charAt(i)=='x'){
			sb.deleteCharAt(i);
			sb.insert(s.length()-1, 'x');
			moveX(sb.toString(),i+1);
		}else{
			moveX(sb.toString(),i+1);
		}
	}
	/*public static void moveX1(String s1,String s2,int i){
		if(i>=s1.length()){
			moveX2(s1,s2,0);
			return;
		}
		if(s1.charAt(i)!='x'){
			s2+=s1.charAt(i);
		}
		moveX1(s1,s2,i+1);
	}
	public static void moveX2(String s1,String s2,int i){
		if(i>=s1.length()){
			System.out.println(s2);
			return;
		}
		if(s1.charAt(i)=='x'){
			s2+=s1.charAt(i);
		}
		moveX2(s1,s2,i+1);
	}*/
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String s1=sc.next();
		String s2="";
		moveX(s1,0);
		/*for(int i=0;i<s1.length();i++){
			if(s1.charAt(i)!='x'){
				s2+=s1.charAt(i);
			}
		}
		for(int i=0;i<s1.length();i++){
			if(s1.charAt(i)=='x'){
				s2+=s1.charAt(i);
			}
		}
		System.out.println(s2);*/
	}

}
