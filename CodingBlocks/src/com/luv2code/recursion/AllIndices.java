package com.luv2code.recursion;

import java.util.Scanner;

public class AllIndices {
	public static void indices(int arr[],int n,int m,int i){
		if(i>=n)
			return;
		if(arr[i]==m){
			System.out.print(i+" ");
		}
		indices(arr,n,m,i+1);
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n=sc.nextInt();
		int arr[] = new int[n];
		for(int i=0;i<n;i++){
			arr[i]=sc.nextInt();
		}
		int m=sc.nextInt();
		indices(arr,n,m,0);
	}
}
