package com.luv2code.recursion;

import java.util.Scanner;

public class IsReverse {
	public static void checkReverse(String s1,String s2,int i,int j){
		if(i>=s1.length()){
			System.out.println("true");
			return;
		}
		if(s1.charAt(i)!=s2.charAt(j))
		{
			System.out.println("false");
			return;
		}
		checkReverse(s1,s2,i+1,j-1);
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String s1=sc.next();
		String s2=sc.next();
		checkReverse(s1,s2,0,s2.length()-1);
	}

}
