package com.luv2code.recursion;

import java.util.Scanner;

public class Twins {
	public static void twins(String s,int i,int count){
		if(i==s.length()-2){
			System.out.println(count);
			return;
		}	
		if(s.charAt(i)==s.charAt(i+2)){
			count++;
		}
		twins(s,i+1,count);
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String s = sc.next();
		twins(s,0,0);
	}

}
