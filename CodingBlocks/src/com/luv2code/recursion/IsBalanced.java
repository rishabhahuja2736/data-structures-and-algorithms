package com.luv2code.recursion;

import java.util.ArrayList;
import java.util.Scanner;

public class IsBalanced {
	public static boolean balanced(String s,ArrayList<Character> al){
		if(s.length()<1){
			if(al.isEmpty())
				return true;
			return false;
		}
		if(isOpenParenthesis(s.charAt(0))){
			al.add(s.charAt(0));
			return balanced(s.substring(1),al);
			
		}
		if(isCloseParenthesis(s.charAt(0))){
			if(al.isEmpty()){
				return false;
			}
			char ch=al.get(al.size()-1);
			al.remove(al.get(al.size()-1));
			if(!matchingParenthesis(ch,s.charAt(0))){
				return false;
			}
			return balanced(s.substring(1),al);

		}
		return balanced(s.substring(1),al);
		
	}
	public static boolean isOpenParenthesis(char ch){
		String s="{[(";
		if(s.indexOf(ch)!=-1)
			return true;
		return false;
	}
	public static boolean isCloseParenthesis(char ch){
		String s="}])";
		if(s.indexOf(ch)!=-1)
			return true;
		return false;
	}
	public static boolean matchingParenthesis(char ch1,char ch2){
		if(ch1=='('&&ch2==')')
			return true;
		if(ch1=='{'&&ch2=='}')
			return true;
		if(ch1=='['&&ch2==']')
			return true;
		return false;
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String s = sc.next();
		ArrayList<Character> al = new ArrayList<>();
		System.out.println(balanced(s,al));
	}

}
