package com.luv2code.recursion;

import java.util.Scanner;

public class CountRemoveReplace2 {
	public static void crr1(String s,int i,int count){
		if(i>=s.length()-1){
			System.out.println(count);
			return;
		}
		if(s.charAt(i)=='h'&&s.charAt(i+1)=='i')
			count++;
		crr1(s,i+1,count);
	}
	public static void crr2(String s,int i){
		StringBuilder sb= new StringBuilder(s);
		if(i==s.length()-1){
			System.out.println(sb.toString());
			return;
		}
		if(s.charAt(i)=='h'&&s.charAt(i+1)=='i'){
			sb.delete(i,i+2);
			crr2(sb.toString(),0);
			return;
		}
		crr2(sb.toString(),i+1);
	}
	public static void crr3(String s,int i){
		StringBuilder sb= new StringBuilder(s);
		if(i>=s.length()-1){
			System.out.println(sb.toString());
			return;
		}
		if(s.charAt(i)=='h'&&s.charAt(i+1)=='i'){
			sb.replace(i,i+2,"bye");
		}
		crr3(sb.toString(),i+1);
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String s = sc.next();
		crr1(s,0,0);
		crr2(s,0);
		crr3(s,0);
	}

}
