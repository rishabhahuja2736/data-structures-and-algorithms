package com.luv2code.recursion;

import java.util.Scanner;

public class NthTriangle {
	public static void triangle(int n,int i,int sum){
		if(i>n){
			System.out.println(sum);
			return;
		}
		sum=sum+i;
		triangle(n,i+1,sum);
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n= sc.nextInt(),sum=0;
		triangle(n,1,sum);
		/*int n= sc.nextInt(),sum=0;
		for(int i=1;i<=n;i++){
			sum=sum+i;
		}
		System.out.println(sum);*/
	}

}
