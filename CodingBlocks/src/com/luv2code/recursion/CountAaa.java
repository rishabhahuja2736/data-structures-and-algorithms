package com.luv2code.recursion;

import java.util.Scanner;

public class CountAaa {
	public static void count1(String s,int i,int count){
		if(i>=s.length()-2){
			System.out.println(count);
			return;
		}
		if(s.charAt(i)=='a'&&s.charAt(i+1)=='a'&&s.charAt(i+2)=='a'){
			count++;
		}
		count1(s,i+1,count);
	}
	public static void count2(String s,int i,int count){
		if(i>=s.length()-2){
			System.out.println(count);
			return;
		}
		if(s.charAt(i)=='a'&&s.charAt(i+1)=='a'&&s.charAt(i+2)=='a'){
			count++;
		}
		count2(s,i+3,count);
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String s = sc.next();
		count1(s,0,0);
		count2(s,0,0);
	}
}
