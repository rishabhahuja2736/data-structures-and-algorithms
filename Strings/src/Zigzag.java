//http://www.geeksforgeeks.org/convert-array-into-zig-zag-fashion/
import java.util.Arrays;
import java.util.Scanner;
public class Zigzag{
	public static void zigzag(int arr[]){
		boolean flag=true;
		for(int i=0;i<(arr.length-1);i++){
			if(flag){
				if(arr[i]>arr[i+1]){
					int temp;
					temp=arr[i];
					arr[i]=arr[i+1];
					arr[i+1]=temp;
				}
			}
			else{
				if(arr[i]<arr[i+1]){
					int temp;
					temp=arr[i];
					arr[i]=arr[i+1];
					arr[i+1]=temp;
				}
			}
			flag=!flag;
		}
		for(int k=0;k<arr.length;k++){
			System.out.print(arr[k]+" ");
		}
	}
	public static void main(String args[]){
		Scanner sc = new Scanner(System.in);
		int n=sc.nextInt();
		int arr[] = new int[n];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		zigzag(arr);
	}
}
