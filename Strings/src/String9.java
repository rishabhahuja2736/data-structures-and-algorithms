//http://www.geeksforgeeks.org/minimum-length-subarray-sum-greater-given-value/
//Smallest subarray with sum greater than a given value
import java.util.*;
public class String9 {
	public static void sum(int arr[],int x){
		int start=0,end=0,min=Integer.MAX_VALUE;
		int sum;
		for(int i=0;i<arr.length;i++){
			sum=0;
			//System.out.println(min);
			for(int j=i;j<arr.length&&(j-i+1)<min;j++){
				sum=sum+arr[j];
				if(sum>x){
						start=i;
						end=j;
						min=end-start+1;
					break;
				}
			}
			//System.out.println(sum);
		}
		for(int i=start;i<=end;i++){
			System.out.println(arr[i]+" ");
		}
	}
	public static void main(String[] args) {
		Scanner sc= new Scanner(System.in);
		int x=sc.nextInt();
		int n=sc.nextInt();
		int a[] = new int[n];
		for(int i=0;i<a.length;i++){
			a[i]=sc.nextInt();
		}
		sum(a,x);
	}

}
