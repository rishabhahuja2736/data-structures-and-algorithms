//http://www.geeksforgeeks.org/count-triplets-with-sum-smaller-that-a-given-value/
//efficient compexity o(n^2) 
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;
public class Triplets2 { 
	public static int triplets(int a[],int sum){
		int count=0,j,k;
		Arrays.sort(a);
		int n=a.length;
		for(int i=0;i<n-2;i++){
			j=i+1;
			k=n-1;
			while(j<k){
				if(a[i]+a[j]+a[k]>=sum){
					k--;
				}else{
					count += (k - j);
					j++;
				}
			}
		}
		return count;
	}
	public static void main(String args[]){
		Scanner sc = new Scanner(System.in);
		int n=sc.nextInt();
		int a[] = new int[n];
		int sum=sc.nextInt();
		for(int i=0;i<n;i++){
			a[i]=sc.nextInt();
		}
		System.out.println(triplets(a,sum));
	}
}
