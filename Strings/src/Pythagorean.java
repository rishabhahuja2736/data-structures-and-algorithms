//http://www.geeksforgeeks.org/find-pythagorean-triplet-in-an-unsorted-array/
import java.util.Scanner;
public class Pythagorean {
	public static boolean triplets(int a[],int sum){
		int count=0;
		int n=a.length;
		for(int i=0;i<n-2;i++){
			for(int j=i+1;j<n-1;j++){
				for(int k=j+1;k<n;k++){
					double x=Math.sqrt(i);
					double y=Math.sqrt(i);
					double z=Math.sqrt(i);
					if(x==y+z||y==x+z||z==x+y){
						return true;
					}
				}
			}
		}
		return false;
	}
	public static void main(String args[]){
		Scanner sc = new Scanner(System.in);
		int n=sc.nextInt();
		int a[] = new int[n];
		int sum=sc.nextInt();
		for(int i=0;i<n;i++){
			a[i]=sc.nextInt();
		}
		System.out.println(triplets(a,sum));
	}
}
