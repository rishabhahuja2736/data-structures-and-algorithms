
//http://www.ideserve.co.in/learn/buy-and-sell-stock-part-one
//http://www.geeksforgeeks.org/stock-buy-sell/
import java.util.Arrays;
import java.util.Scanner;

public class String10 {
	static int mincp = Integer.MAX_VALUE;
	static int profit = 0;
	static int sp, cp;

	public static void stocks(int arr[]) {
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] < mincp) {
				mincp = arr[i];
			}
			// System.out.println(mincp);
			if (arr[i] - mincp > profit) {
				profit = arr[i] - mincp;
				// System.out.println(profit);
				sp = i;
				cp = getArrayIndex(arr, mincp);
			}
		}
		System.out.println("Buy on day-:" + cp + " Sell on day:-" + sp);
	}

	public static int getArrayIndex(int[] arr, int value) {

		int k = 0;
		for (int i = 0; i < arr.length; i++) {

			if (arr[i] == value) {
				k = i;
				break;
			}
		}
		return k;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int arr[] = new int[n];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = sc.nextInt();
		}
		stocks(arr);
	}
}
