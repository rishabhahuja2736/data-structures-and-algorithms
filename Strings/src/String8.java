//http://www.geeksforgeeks.org/top-10-algorithms-in-interview-questions/
// Find the smallest positive integer value that cannot be represented as sum of any subset of a given array
import java.util.*;
public class String8 {
	public static int subarr(int arr[]){
		int res=1;
		for(int i=0;i<arr.length&&res>=arr[i];i++){
			res=res+arr[i];
		}
		return res;
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n=sc.nextInt();
		int arr[] = new int[n];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println(subarr(arr));
	}

}
