//http://www.geeksforgeeks.org/count-triplets-with-sum-smaller-that-a-given-value/
import java.util.Scanner;
public class Triplets {
	public static int triplets(int a[],int sum){
		int count=0;
		int n=a.length;
		for(int i=0;i<n-2;i++){
			for(int j=i+1;j<n-1;j++){
				for(int k=j+1;k<n;k++){
					if(a[i]+a[j]+a[k]<sum){
						count++;
					}
				}
			}
		}
		return count;
	}
	public static void main(String args[]){
		Scanner sc = new Scanner(System.in);
		int n=sc.nextInt();
		int a[] = new int[n];
		int sum=sc.nextInt();
		for(int i=0;i<n;i++){
			a[i]=sc.nextInt();
		}
		System.out.println(triplets(a,sum));
	}
}
