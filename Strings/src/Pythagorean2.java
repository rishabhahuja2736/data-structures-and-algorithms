import java.util.Arrays;
import java.util.Scanner;

public class Pythagorean2 {
	public static boolean triplets(int a[]){
		int count=0,j,k;
		Arrays.sort(a);
		int n=a.length;
		for(int i=0;i<n-2;i++){
			j=i+1;
			k=n-1;
			while(j<k){
				int x=a[i]*a[i];
				int y=a[j]*a[j];
				int z=a[k]*a[k];
				if(x+y==z){
					return true;
				}
				else if(x+y>z){
					j++;
				}else{
					k--;
				}
			}
		}
		return false;
	}
	public static void main(String args[]){
		Scanner sc = new Scanner(System.in);
		int n=sc.nextInt();
		int a[] = new int[n];
		for(int i=0;i<n;i++){
			a[i]=sc.nextInt();
		}
		System.out.println(triplets(a));
	}
}
