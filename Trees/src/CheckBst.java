//https://www.youtube.com/watch?v=MILxfAbIhrE&list=PLrmLmBdmIlpv_jNDXtJGYTPNQ2L1gdHxu&index=8
public class CheckBst {
	int max=Integer.MAX_VALUE;
	int min=Integer.MIN_VALUE;
	Node root;
	static class Node{
		Node left,right;
		int data;
		Node(int data){
			this.data=data;
			left=right=null;
		}
	}
	CheckBst(){
		root=null;
	}
	public boolean check(Node root,int min,int max){
		if(root==null)
			return true;
		if(root.data<=min||root.data>max){
			return false;
		}
		return check(root.left,min,root.data)&&check(root.right,root.data,max);
	}
	public static void main(String[] args) {
		CheckBst c = new CheckBst();
		c.root=new Node(10);
		c.root.left= new Node(-10);
		c.root.right= new Node(19);
		c.root.left.left=new Node(-20);
		c.root.left.right=new Node(0);
		c.root.right=new Node(19);
		c.root.right.left=new Node(17);
		System.out.println(c.check(c.root,c.min,c.max));
	}

}
