//https://www.youtube.com/watch?v=qjD-CmuC0MQ
//http://www.geeksforgeeks.org/print-ancestors-of-a-given-node-in-binary-tree
public class Ancestor {
	Node root;

	static class Node {
		Node left, right;
		int data;

		Node(int data) {
			left = right = null;
			this.data = data;
		}
	}

	public boolean ancestor(Node node, int key) {
		if (node == null) {
			return false;
		}
		if (node.data == key)
			return true;
		if (ancestor(node.left, key) || ancestor(node.right, key)) {
			System.out.println(node.data);
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Ancestor a = new Ancestor();

		a.root = new Node(1);
		a.root.left = new Node(2);
		a.root.right = new Node(3);
		a.root.left.left = new Node(4);
		a.root.left.right = new Node(5);
		a.ancestor(a.root, 5);
	}

}
