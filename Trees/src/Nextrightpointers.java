//http://www.geeksforgeeks.org/connect-nodes-at-same-level/
import java.util.*;

public class Nextrightpointers {
	Node root;

	static class Node {
		Node left, right;
		int data;

		Node(int data) {
			this.data = data;
			left = right = null;
		}
	}

	Nextrightpointers() {
		root = null;
	}

	public void nextpointer(Node node) {
		if (node == null)
			return;
		int count, result = 0;
		Queue<Node> q = new LinkedList<>();
		q.add(root);
		while (!q.isEmpty()) {
			count = q.size();
			while (count > 0) {
				Node current = q.remove();
				if (current.left != null) {
					q.add(current.left);
				}
				if (current.right != null) {
					q.add(current.right);
				}
				if (current.data != root.data && count > 1) {
					current.right = q.peek();
				}
				if (count == 1) {
					current.right = null;
				}
				count--;
			}
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Nextrightpointers n = new Nextrightpointers();
		n.root = new Node(1);
		n.root.left = new Node(2);
		n.root.right = new Node(3);
		n.root.left.left = new Node(4);
		n.root.left.right = new Node(5);
		n.nextpointer(n.root);
		System.out.println(n.root.left.left.right.data);
	}

}
