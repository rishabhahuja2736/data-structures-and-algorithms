//https://www.youtube.com/watch?v=NA8B84DZYSA&list=PLrmLmBdmIlpv_jNDXtJGYTPNQ2L1gdHxu&index=5
public class Size {
	Node root;
	static class Node{
		Node left;
		Node right;
		int data;
		Node(int data){
			this.data=data;
			left=right=null;
		}
	}
	Size(){
		root=null;
	}
	public int size(Node node){
		if(node==null)
			return 0;
		int left=size(node.left);
		int right=size(node.right);
		return left+right+1;
	}
	public static void main(String args[]){
		Size s = new Size();
		s.root=new Node(1);
		s.root.left=new Node(2);
		s.root.right=new Node(3);
		s.root.right.left=new Node(4);
		s.root.right.right=new Node(5);
		System.out.println(s.size(s.root));
	}
}
