
//http://www.geeksforgeeks.org/inorder-tree-traversal-without-recursion/
import java.util.*;

public class InorderIterative {
	Node root;

	static class Node {
		int data;
		Node left, right;

		Node(int data) {
			this.data = data;
		}
	}

	public void iterative(Node node) {
		if (node == null)
			return;
		Stack<Node> s = new Stack<Node>();
		while (node != null) {
			s.push(node);
			node = node.left;
		}
		while (s.size() > 0) {
			node = s.pop();
			System.out.println(node.data);
			if (node.right != null) {
				node = node.right;

				while (node != null) {
					s.push(node);
					node = node.left;
				}
			}
		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		InorderIterative i = new InorderIterative();
		i.root = new Node(1);
		i.root.left = new Node(2);
		i.root.right = new Node(3);
		i.root.left.left = new Node(4);
		i.root.left.right = new Node(5);
		i.iterative(i.root);
	}

}
