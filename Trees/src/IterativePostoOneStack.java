
//https://www.youtube.com/watch?v=qT65HltK2uE&list=PLrmLmBdmIlpv_jNDXtJGYTPNQ2L1gdHxu&index=10
import java.util.*;

public class IterativePostoOneStack {
	Node root = null;

	static class Node {
		Node left, right;
		int data;

		Node(int data) {
			this.data = data;
			left = right = null;
		}
	}

	IterativePostoOneStack() {
		root = null;
	}

	public void traverse(Node node) {
		if (node == null)
			return;
		Stack<Node> s1 = new Stack<>();
		Stack<Node> s2 = new Stack<>();
		s1.push(node);
		while (!s1.isEmpty()) {
			Node current = s1.pop();
			if (current.left != null)
				s1.push(current.left);
			if (current.right != null)
				s1.push(current.right);
			s2.push(current);
		}
		while (!s2.isEmpty()) {
			System.out.println(s2.pop().data);
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		IterativePostoOneStack s = new IterativePostoOneStack();
		s.root = new Node(1);
		s.root.left = new Node(2);
		s.root.right = new Node(3);
		s.root.right.left = new Node(4);
		s.root.right.right = new Node(5);
		s.traverse(s.root);
		;
	}

}
