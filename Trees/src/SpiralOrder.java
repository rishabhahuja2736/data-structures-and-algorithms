//https://www.youtube.com/watch?v=vjt5Y6-1KsQ&index=15&list=PLrmLmBdmIlpv_jNDXtJGYTPNQ2L1gdHxu
import java.util.*;
public class SpiralOrder {
	Node root;
	static class Node{
		Node left,right;
		int data;
		Node(int data){
			this.data=data;
			left=right=null;
		}
	}
	SpiralOrder(){
		root=null;
	}
	public void traversal(Node root){
		Stack<Node> s1 = new Stack<>();
		Stack<Node> s2 = new Stack<>();
		s1.push(root);
		while(!s1.isEmpty()||!s2.isEmpty()){
			while(!s1.isEmpty()){
				if(root==null)
					return;
				Node current=s1.pop();
				System.out.println(current.data);
				if(current.left!=null){
					s2.push(current.left);
				}
				if(current.right!=null){
					s2.push(current.right);
				}
			}
			while(!s2.isEmpty()){
				if(root==null)
					return;
				Node current=s2.pop();
				System.out.println(current.data);
				if(current.right!=null){
					s1.push(current.right);	
				}
				if(current.left!=null){
					s1.push(current.left);
				}
			}
		}
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SpiralOrder d = new SpiralOrder();
		d.root = new Node(1);
		d.root.left = new Node(2);
		d.root.right = new Node(3);
		d.root.left.left = new Node(4);
		d.root.left.right = new Node(5);
		d.traversal(d.root);
	}

}
