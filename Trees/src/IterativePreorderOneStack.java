//https://www.youtube.com/watch?v=elQcrJrfObg&list=PLrmLmBdmIlpv_jNDXtJGYTPNQ2L1gdHxu&index=11
import java.util.*;
public class IterativePreorderOneStack {
	Node root;
	static class Node{
		Node left,right;
		int data;
		Node(int data){
			left=right=null;
			this.data=data;
		}
	}
	IterativePreorderOneStack(){
		root=null;
	}
	public void traverse(Node root){
		if(root==null)
			return; 
		Stack<Node> s = new Stack<>();
		s.push(root);
		while(!s.isEmpty()){
			Node current=s.pop();
			System.out.println(current.data);
			if(current.right!=null){
				s.push(current.right);
			}
			if(current.left!=null){
				s.push(current.left);
			}
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		IterativePreorderOneStack d = new IterativePreorderOneStack();
		d.root = new Node(1);
		d.root.left = new Node(2);
		d.root.right = new Node(3);
		d.root.left.left = new Node(4);
		d.root.left.right = new Node(5);
		d.traverse(d.root);
	}

}
