//http://www.geeksforgeeks.org/binary-tree-set-1-introduction/
public class BinaryTree {
	Node root;
	public class Node{
		int data;
		Node left,right;
		Node(int data){
			this.data=data;
			left=null;
			right=null;
		}
	}
	public void insert(Node node,int data){
		Node newnode = new Node(data);
		if(root==null){
			root=newnode;
			return;
		}
		if(node.left!=null&&node.right!=null){
			insert(node.left,data);
		}
		else if(node.right!=null){
			insert(node.right,data);
		}
		else if(node.left==null&&node.right==null){
			node.left=newnode;
		}
		else if(node.left!=null&&node.right==null){
			node.right=newnode;
		}
	}
	public void print(Node node){
		/*while(node!=null){
			//print(node.left);
			System.out.println(node.data);
			node=node.left;
			//print(node.right);
		}*/
		if(node!=null){
			System.out.println(node.data);
			print(node.right);
			
		}
	}
	public static void main(String args[]){
		BinaryTree b = new BinaryTree();
		b.insert(b.root,2);
		b.insert(b.root,3);
		b.insert(b.root,4);
		b.insert(b.root,5);
		b.insert(b.root,6);
		b.insert(b.root,7);
		b.insert(b.root,8);
		System.out.println(b.root.left.left.left.data);
		//b.print(b.root);
	}
}