//https://www.youtube.com/watch?v=nzmtCFNae9k&list=PLrmLmBdmIlpv_jNDXtJGYTPNQ2L1gdHxu&index=12
import java.util.*;
public class IterativeInorderOneStack {
	Node root;
	static class Node{
		Node right,left;
		int data;
		Node(int data){
			this.data=data;
			right=left=null;
		}
	}
	IterativeInorderOneStack(){
		root=null;
	}
	public void traverse(Node root){
		if(root==null)
			return;
		Stack<Node> s = new Stack<>();
		while(true){
			if(root!=null){
				s.add(root);
				root=root.left;
			}else{
				if(s.isEmpty())
					break;
				root=s.pop();
				System.out.println(root.data);
				root=root.right;
			}
		}
	}
	public static void main(String[] args) {
		IterativeInorderOneStack i = new IterativeInorderOneStack();
		i.root = new Node(1);
		i.root.left = new Node(2);
		i.root.right = new Node(3);
		i.root.left.left = new Node(4);
		i.root.left.right = new Node(5);
		i.traverse(i.root);
	}

}
