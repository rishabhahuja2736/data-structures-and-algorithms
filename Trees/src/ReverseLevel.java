//https://www.youtube.com/watch?v=D2bIbWGgvzI&index=14&list=PLrmLmBdmIlpv_jNDXtJGYTPNQ2L1gdHxu
import java.util.*;
public class ReverseLevel {
	Node root;
	static class Node{
		int data;
		Node left,right;
		Node(int data){
			this.data=data;
			left=right=null;
		}
	}
	ReverseLevel(){
		root=null;
	}
	public void check(Node root){
		if(root==null)
			return;
		Queue<Node> q = new LinkedList<>();
		Stack<Node> s = new  Stack<>();
		q.add(root);
		while(!q.isEmpty()){
			Node current=q.remove();
			if(current.right!=null){
				q.add(current.right);
			}
			if(current.left!=null){
				q.add(current.left);
			}
			s.add(current);
		}
		while(!s.isEmpty())
			System.out.println(s.pop().data);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ReverseLevel r = new ReverseLevel();
		r.root=new Node(1);
		r.root.left=new Node(2);
		r.root.right= new Node(3);
		r.root.left.left=new Node(4);
		r.root.left.right=new Node(5);
		r.check(r.root);
	}

}
