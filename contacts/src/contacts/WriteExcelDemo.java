package contacts;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.DriverManager;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.ResultSet;
import com.mysql.jdbc.Statement;

public class WriteExcelDemo 
{
    public static void main(String[] args) 
    {
    	 int i = 1;
    	 TreeMap<Integer, Object[]> data = new TreeMap<Integer, Object[]>(); 
    	try{  
    		Class.forName("com.mysql.jdbc.Driver");  
    		  
    		Connection con=(Connection) DriverManager.getConnection(  "jdbc:mysql://localhost:3306/astrea","root","123");  
    		  
    		
    		Statement stmt=(Statement) con.createStatement();  
    		  
    		ResultSet rs=(ResultSet) stmt.executeQuery("select * from contacts");  
    		 
    		while(rs.next())
    		{  
    			data.put(i, new Object[] {rs.getInt(1), rs.getString(2), rs.getString(3)});
    			i++;
    		}
    		con.close();
    		  
    		  
    		}catch(Exception e){ System.out.println(e);} 
    	
    		  
    		
        //Blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook(); 
         
        //Create a blank sheet
        XSSFSheet sheet = workbook.createSheet("Employee Data");
      
        //Iterate over data and write to sheet
       
		Set<Integer> keyset = data.keySet();
        int rownum = 0;
        for (int key : keyset)
        {
            Row row = sheet.createRow(rownum++);
            Object [] objArr = data.get(key);
            //System.out.println(objArr);
            int cellnum = 0;
            for (Object obj : objArr)
            {
               Cell cell = row.createCell(cellnum++);
               if(obj instanceof String){
            	   	
                    cell.setCellValue((String)obj);}
                else if(obj instanceof Integer){
                	
                    cell.setCellValue((Integer)obj);}
            }
        }
        try
        {
            //Write the workbook in file system
            FileOutputStream out = new FileOutputStream(new File("/home/rishabh/howtodoinjava_demo1.xlsx"));
            workbook.write(out);
            out.close();
            System.out.println("howtodoinjava_demo.xlsx written successfully on disk.");
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
    }
}