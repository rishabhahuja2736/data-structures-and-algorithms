//http://www.ideserve.co.in/learn/find-an-element-in-a-sorted-rotated-array
public class FindElementSortedRotatedArray {
	public int findElement(int arr[],int num){
		if(arr==null||arr.length==0){
			return -1;
		}
		int pivot=findPivot(arr,0,arr.length-1);
		System.out.println(pivot);
		if(pivot>0&&num >= arr[0] && num <= arr[pivot - 1]){
			return binarySearch(arr,0,pivot-1,num);
		}else{
			return binarySearch(arr,pivot,arr.length-1,num);
		}
	}
	public int binarySearch(int arr[],int first,int end,int num){
		if(first>end){
			return -1;
		}
		int mid=(first+end)/2;
		if(num==arr[mid]){
			return mid;
		}else if(num<arr[mid-1]){
			return binarySearch(arr,first,mid-1,num);
		}else{
			return binarySearch(arr,mid+1,end,num);
		}
	}
	public int findPivot(int arr[],int first,int end){
		if(arr.length==1||arr[0]<arr[end]){
			return 0;
		}
		if(arr==null||arr.length==0){
			return -1;
		}
		if(first>end){
			return 0;
		}
		int mid;
		mid=(first+end)/2;
		if(arr[mid]>arr[mid+1]){
			return mid+1;
		}else if(arr[first]<=arr[mid]){
			first=mid+1;
			return findPivot(arr,first,end);
		}else{
			end=mid-1;
			return findPivot(arr,first,end);
		}
	}
	public static void main(String[] args) {
		 FindElementSortedRotatedArray a = new FindElementSortedRotatedArray();
		 int array[] = { 156, 235, 457, 21, 32, 43, 74, 75, 86, 97, 108, 149 };
		 System.out.println("Index of the searched element is "+a.findElement(array, 43));
	}

}
