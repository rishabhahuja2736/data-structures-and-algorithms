//https://www.youtube.com/watch?v=gcULXE7ViZw&list=PL2_aWCzGMAwI3W_JlcBbtYTwiQSsOTa6P&index=36
public class Delete {
	Node root;

	static class Node {
		Node left, right;
		int data;

		Node(int data) {
			this.data = data;
			left = right = null;
		}
	}
	
	Delete(){
		root=null;
	}

	public Node delete(Node node, int data) {
		if (node == null)
			return node;
		else if (data < node.data) {
			node.left = delete(node.left, data);
		} else if (data > node.data) {
			node.right = delete(node.right, data);
		} else {
			if (node.left == null && node.right == null) {
				node = null;
			} else if (node.left == null) {
				node = node.right;
			} else if (node.right == null) {
				node = node.left;
			} else {
				Node temp = findmin(node.right);
				node.data = temp.data;
				node.right = delete(node.right, temp.data);
			}
		}
		return node;
	}

	public Node insert(Node node, int data) {
		Node newnode = new Node(data);
		if (root == null) {
			root = newnode;
			return root;
		}
		if (node == null) {
			node = newnode;
		} else if (data <= node.data) {
			node.left = insert(node.left, data);
		} else if (data > node.data) {
			node.right = insert(node.right, data);
		}
		return node;
	}

	public Node findmin(Node node) {
		if (node == null)
			return node;
		if (node.left == null) {
			return node;
		}
		return findmin(node.left);
	}
	
	public void inorder(Node node){
		if(node==null)
			return;
		inorder(node.left);
		System.out.println(node.data);
		inorder(node.right);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Delete m = new Delete();
		m.insert(m.root, 50);
		m.insert(m.root, 30);
		m.insert(m.root, 20);
		m.insert(m.root, 40);
		m.insert(m.root, 70);
		m.insert(m.root, 60);
		m.insert(m.root, 80);
		m.inorder(m.root);
		System.out.println("-------------");
		m.delete(m.root, 70);
		m.inorder(m.root);
		
		
	}

}
