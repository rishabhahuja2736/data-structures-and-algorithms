//https://www.youtube.com/watch?v=wBFttOncVUc
//http://www.geeksforgeeks.org/binary-tree-to-binary-search-tree-conversion/
import java.util.Arrays;
public class Binarttobst {
	Node root;
	int i = 0;
	int arr[] = new int[9];

	static class Node {
		int data;
		Node left, right;

		Node(int data) {
			this.data = data;
			left = right = null;
		}
	}

	Binarttobst() {
		root = null;
	}

	public void convert(Node node) {
		createInorderArray(node);
		Arrays.sort(arr);
		i = 0;
		binaryToBst(node);
	}

	public void binaryToBst(Node node) {
		if (node == null)
			return;
		binaryToBst(node.left);
		node.data = arr[i++];
		binaryToBst(node.right);
	}

	public void createInorderArray(Node node) {
		if (node == null) {
			return;
		}
		createInorderArray(node.left);
		arr[i++] = node.data;
		createInorderArray(node.right);
	}

	public void inorder(Node root) {
		if (root == null)
			return;
		inorder(root.left);
		System.out.println(root.data);
		inorder(root.right);
	}

	public static void main(String[] args) {
		Binarttobst b = new Binarttobst();
		b.root = new Node(0);
		b.root.left = new Node(1);
		b.root.left.left = new Node(3);
		b.root.left.right = new Node(4);
		b.root.left.left.right = new Node(6);
		b.root.left.left.right.right = new Node(8);
		b.root.right = new Node(2);
		b.root.right.left = new Node(5);
		b.root.right.left.right = new Node(7);
		System.out.println("Earlier");
		b.inorder(b.root);
		System.out.println("After");
		b.convert(b.root);
		b.inorder(b.root);

	}

}
