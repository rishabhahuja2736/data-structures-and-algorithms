//https://www.youtube.com/watch?v=TIoCCStdiFo
public class Lca {
	Node root;
	static class Node{
		int data;
		Node left,right;
		Node(int data){
			this.data=data;
			right=left=null;
		}
	}
	Lca(){
		root=null;
	}
	public int  lca(Node node,Node n1,Node n2){
		if(node==null){
			return 0;
		}
		if(node.data>Math.max(n1.data,n2.data)){
			return lca(node.left,n1,n2);
		}
		else if(node.data<Math.min(n1.data,n2.data)){
			return lca(node.right,n1,n2);
		}
		else{
			return node.data;
		}
	}
	public Node insert(Node node, int data) {
		Node newnode = new Node(data);
		if (root == null) {
			root = newnode;
			return root;
		}
		if (node == null) {
			node = newnode;
		} else if (data <= node.data) {
			node.left = insert(node.left, data);
		} else if (data > node.data) {
			node.right = insert(node.right, data);
		}
		return node;
	}
	public static void main(String[] args) {
		Lca a = new Lca();
		
		a.insert(a.root, 50);
		a.insert(a.root, 30);
		a.insert(a.root, 20);
		a.insert(a.root, 40);
		a.insert(a.root, 70);
		a.insert(a.root, 60);
		a.insert(a.root, 80);
		
		System.out.println(a.lca(a.root,a.root.left.left,a.root.left.right));
	}
}
