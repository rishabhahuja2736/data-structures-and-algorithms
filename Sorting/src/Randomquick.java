import java.util.Random;
import java.util.Scanner;

public class Randomquick {
	public int partion(int arr[],int l,int r){
		int i,temp,x;
		x=arr[r];
		i=l-1;
		//System.out.println("---"+x);
		for(int j=l;j<=r-1;j++){
			if(arr[j]<x){
				i++;
				temp=arr[j];
				arr[j]=arr[i];
				arr[i]=temp;
			}
		}
		temp=arr[i+1];
		arr[i+1]=arr[r];
		arr[r]=temp;
		//System.out.println("value is"+arr[i]);
		return i+1;
	}
	public int randomquick(int arr[],int l,int r){
		int x=0,temp,ran,q;
		ran=randInt(l,r);
		temp=arr[ran];
		arr[ran]=arr[r];
		arr[r]=temp;
		q=partion(arr,l,r);
		return q;
	}
	public int randInt(int min, int max) {
	    Random rand =null;
	    int randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}
	public void quick(int arr[],int l,int r){
		int q;
		if(l<r){
			q=randomquick(arr,l,r);
			//System.out.println("value of q= "+q);
			quick(arr,l,q-1);
			quick(arr,q+1,r);
		}
		else{
			return;
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n,l,r;
		Quicksort q = new Quicksort();
		Scanner sc =new Scanner(System.in);
		n=sc.nextInt();
		int arr[] = new int[n];
		for(int i=0;i<=n-1;i++){
			arr[i]=sc.nextInt();
		}
		l=0;
		r=n-1;
		q.quick(arr,l,r);
		for(int i=0;i<=n-1;i++){
			System.out.println(arr[i]);
		}
	}

}
