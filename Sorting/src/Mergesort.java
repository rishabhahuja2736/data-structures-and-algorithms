import java.util.*;
public class Mergesort {

	public static void mergesort(int arr[],int start,int end){
		int mid,k=0;
		mid=(start+end)/2;
		if(arr.length<2){
			return;
		}
		int lsize=mid+1;
		int rsize=(end-mid);
		int left[] =new int[lsize];
		int right[] =new int[rsize];
		for(int i=0;i<lsize;i++){
			left[i]=arr[k++];	
			System.out.println("left"+left[i]);
		}
		for(int j=0;j<rsize;j++){
			right[j]=arr[k++];
			System.out.println("right"+right[j]);
		}
		mergesort(left,start,lsize-1);
		mergesort(right,start,rsize-1);
		merge(left,right,lsize,rsize,arr);
	}
	public static void merge(int left[],int right[],int lsize,int rsize,int arr[]){
		int n=lsize+rsize;
		int temp[] = new int[n];
		int i=0,j=0,k=0,l=0;
		while(i<lsize&&j<rsize){
			if(left[i]<=right[j]){
				//System.out.println("left"+left[i]);
				temp[k++]=left[i++];
				//System.out.println("left"+arr[k]);
			}
			else{
				//System.out.println("right"+right[j]);
				temp[k++]=right[j++];
				//System.out.println("right"+arr[k]);
			}
		}
		while(i<lsize){
			//System.out.println("hello");
			temp[k++]=left[i++];
			//System.out.println("left"+arr[1]);
		}
		while(j<rsize){
			temp[k++]=right[j++];
			//System.out.println("right"+arr[k]);
		}
		for(k=0;k<n;k++){
			arr[l++]=temp[k];
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n;
		Scanner sc = new Scanner(System.in);
		n=sc.nextInt();
		int arr[] = new int[n];
		for(int i=0;i<n;i++){
			arr[i]=sc.nextInt();
		}
		//Mergesort m = new Mergesort(); 
		mergesort(arr,0,arr.length-1);
		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]);
		}
	}

}
