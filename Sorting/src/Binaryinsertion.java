import java.util.Scanner;
public class Binaryinsertion {
	
	public static int binarysearch(int arr[],int x,int first,int last){
		int mid;
		mid=(first+last)/2;
		if(first>last){
			return first;
		}
		if(x==arr[mid]){
			return mid;
		}
		else if(x<arr[mid]){
			last=mid-1;
			return binarysearch(arr,x,first,last);
		}
		else if(x>arr[mid]){
			first=mid+1;
			return binarysearch(arr,x,first,last);
		}
		return 0;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int key,loc;
		int n,temp;
		Scanner sc =new Scanner(System.in);
		n=sc.nextInt();
		int a[] = new int[n];
		for(int i=0;i<n;i++){
			a[i]=sc.nextInt();
		}
		for(int i=1;i<n;i++){
			key=a[i];
			loc=Binaryinsertion.binarysearch(a,key,0,(i-1));
			//int j;
			for(int j=i-1;j>=loc;j--){
				a[j+1]=a[j];
			}
			a[loc]=key;	
		}
		for(int i=0;i<n;i++){
			System.out.println(a[i]);
		}
	}

}
