import java.util.*;
public class Heap<T extends Comparable<T>> {
	ArrayList<T> items;
	public Heap(){
		items=new ArrayList<T>();
	}
	public void siftUp(){
		int k=items.size()-1;
		while(k>0){
			int p=(k-1)/2;
			T item = items.get(k);
			T parent=items.get(p);
			if(item.compareTo(parent)>0){
				//swap
				items.set(p, item);
				items.set(k, parent);
				// move up one level
				k=p;
			}
			else{
				break;
			}
		}
	}
	public void insert(T item){
		items.add(item);
		siftUp();
	}
	public static void main(String[] args) {
		
		Heap<Integer> h = new Heap<Integer>();
		h.insert(15);
		h.insert(10);
		h.insert(9);
		h.insert(8);
		h.insert(9);
		h.insert(6);
		h.insert(3);
		h.insert(4);
		h.insert(2);
		h.insert(12);
		for(Integer i : h.items ){
			System.out.println(i);
		}
	}

}
