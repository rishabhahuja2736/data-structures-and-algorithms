import java.util.Scanner;

public class FirstAndLastDigit {
	public void test(){
		Scanner sc = new Scanner(System.in);
		int t,first=0,length=0;
		double last1;
		t=sc.nextInt();
		int[] a = new int[t];
		int[] b = new int[t];
		for(int i=0;i<t;i++){
			a[i]=sc.nextInt();
		}
		for(int i=0;i<t;i++){
			if(a[i]>0)
			{
				length = (int)(Math.log10(a[i]) + 1);
			}
			else if(a[i]==0)
			{
				length=1;
			}
			first=a[i]%10;
			last1=a[i]/(Math.pow(10,length-1));
			int last=(int)last1;
			b[i]=first+last;
		}
		for(int i=0;i<t;i++){
			System.out.println(b[i]);
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		FirstAndLastDigit f = new FirstAndLastDigit();
		f.test();
	}

}
