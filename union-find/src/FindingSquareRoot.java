import java.util.*;
public class FindingSquareRoot {
	public void test(){
		int t,n,root;
		double sq;
		ArrayList<Integer> al = new ArrayList<Integer>();
		Scanner sc = new Scanner(System.in);
		t=sc.nextInt();
		for(int i=0;i<t;i++){
			n=sc.nextInt();
			sq=Math.sqrt(n);
			root=(int)sq;
			al.add(root);
			
		}
		Iterator i = al.iterator();
		while(i.hasNext()){
			System.out.println(i.next());
		}
	}
	public static void main(String args[]){
		FindingSquareRoot f = new FindingSquareRoot();
		f.test();
	}
}
