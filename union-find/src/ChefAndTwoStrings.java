import java.util.Scanner;

public class ChefAndTwoStrings {
	public void test() {
		int n, al, bl, count1 = 0, count2 = 0;
		String[] s1, s2;

		Scanner sc = new Scanner(System.in);

		n = sc.nextInt();
		int[] a1 = new int[n];
		int[] b1 = new int[n];
		for (int i = 0; i < n; i++) {
			String a = sc.next();
			String b = sc.next();
			for (int j = 0, k = 0; j < a.length() && k < b.length(); j++, k++) {
				int c1 = a.charAt(j);
				int c2 = b.charAt(k);
				if ((c1 == 63 && c2 == 63) || c1 == 63 || c2 == 63 || c1 != c2) {
					count1++;
					a1[i] = count1;

				}

				if (c1 != c2 && c1 != 63 && c2 != 63) {
					count2++;
					b1[i] = count2;
				}

			}
			count1 = 0;
			count2 = 0;
		}
		for (int i = 0; i < n; i++) {
			System.out.print(b1[i] + " ");
			System.out.print(a1[i]);
			System.out.println();
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ChefAndTwoStrings c = new ChefAndTwoStrings();
		c.test();
	}
}
