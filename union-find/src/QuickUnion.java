import java.util.Scanner;

public class QuickUnion {
	int a[];
	public QuickUnion(int n){
		a= new int[n];
		for(int i=0;i<a.length;i++){
			a[i]=i;
		}
	}
	public int root(int i){
		while(i!=a[i]){
			i=a[i];
		}
		return i;
	}
	public boolean connected(int p,int q){
		return root(p)==root(q);
	}
	public void union(int p,int q){
		int proot=root(p);
		int qroot=root(q);
		a[proot]=qroot;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int N=sc.nextInt();
		QuickUnion uf  = new QuickUnion(N);
		int p=sc.nextInt();
		int q=sc.nextInt();
		
		if(!uf.connected(p, q)){
			uf.union(p,q);
		}
		for(int i=0;i<N;i++){
			
			System.out.println(uf.a[i]);			
		}
	}

}
