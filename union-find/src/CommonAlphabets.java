import java.util.LinkedHashSet;

public class CommonAlphabets {
	public void test() {
		String s1 = new String("AMITABH BACHCHAN");
		String s2 = new String("RAJNIKANTH");
		LinkedHashSet<Character> l1 = new LinkedHashSet<Character>();
		LinkedHashSet<Character> l2 = new LinkedHashSet<Character>();
		for (int i = 0; i < s1.length(); i++) {
			l1.add(s1.charAt(i));
		}
		for (int i = 0; i < s2.length(); i++) {
			l2.add(s2.charAt(i));
		}
		l1.retainAll(l2);
		for (Character c : l1) {
			System.out.println(c);
		}
	}

	public static void main(String[] args) {
		CommonAlphabets a = new CommonAlphabets();
		a.test();
	}
}
