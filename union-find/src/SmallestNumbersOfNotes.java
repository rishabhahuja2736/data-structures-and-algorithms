import java.util.*;
public class SmallestNumbersOfNotes {
	public void test(){
		int t,rem=0,n;
		Scanner sc = new Scanner(System.in);
		t=sc.nextInt();
		int[] a = new int[]{1,2,5,10,50,100};
		int[] b = new int[t];
		for(int i=0;i<t;i++){
			n=sc.nextInt();
			for(int j=5;j>=0;j--){
				rem=rem+n/a[j];
				n=n%a[j];
			}
			b[i]=rem;
			rem=0;
		}
		for(int i=0;i<t;i++){
			System.out.println(b[i]);
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SmallestNumbersOfNotes s = new SmallestNumbersOfNotes();
		s.test();
	}

}
