import java.util.Scanner;

public class UnionFind {
	static int[] array;
	public UnionFind(int N){
		array = new int[N];
		for(int i=0;i<array.length;i++){
			array[i]=i;			
		}
	}
	public boolean connected(int p,int q){
		return array[p]==array[q];
	}
	
	public void union(int p,int q){
		int pid=array[p];
		int qid=array[q];
		for(int i=0;i<array.length;i++){
			if(array[i]==pid){
				array[i]=qid;
			}
		}
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int N=sc.nextInt();
		UnionFind uf  = new UnionFind(N);
		int p=sc.nextInt();
		int q=sc.nextInt();
		
		if(!uf.connected(p, q)){
			uf.union(p,q);
		}
		System.out.println(p+" "+q);
		for(int i=0;i<N;i++){
			
			System.out.println(array[i]);			
		}
	}

}
