
import java.util.Scanner;

public class QuickFind {
	int a[];
	int b;
	public QuickFind(int n){
		a = new int[n];
		for(int i=0;i<n;i++){
			a[i]=i;
		}
	}
	public boolean connected(int p,int q){
		return a[p]==a[q];
	}
	public void union(int p,int q){
		int pid=a[p];
		int qid=a[q];
		for(int i=0;i<a.length;i++){
			if(pid==a[i])
				a[i]=qid;
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int p,q,n;
		n=sc.nextInt();
		p=sc.nextInt();
		q=sc.nextInt();
		QuickFind qf =new QuickFind(n);
		if(!qf.connected(p,q)){
			qf.union(p,q);
			for(int i=0;i<qf.a.length;i++)
				System.out.println(qf.a[i]);
		}
	}
	
}
