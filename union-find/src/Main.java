//tcs mockvita problem C (velocity)
import java.util.Scanner;

public class Main {
	
	public int test(){
		Scanner sc = new Scanner(System.in);
		double x,y,z,va,vb,dold,dnew;
		x=sc.nextDouble();
		y=sc.nextDouble();
		va=sc.nextDouble();
		vb=sc.nextDouble();
		dold=x+y;
		if(x<0||y<0||va<0||vb<0){
			System.out.println("invalid input");
			return 0;
		}
		while(true){
			x=x-va;
			y=y-vb;
			z=x*x+y*y;
			dnew=Math.sqrt(z);
			if(dnew>dold){
				if(dold==0){
					System.out.printf("%.1f",dold);
				}else
				{
				System.out.printf("%.11f",dold);
				}
				break;
			}
			dold=dnew;
			
		}
		return 0;
		
	}
	
	public static void main(String[] args) throws java.lang.Exception {
		
		Main m = new Main();
		m.test();
	}

}
