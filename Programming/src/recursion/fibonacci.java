package recursion;

public class fibonacci {
	public int sum(int n){
		int temp;
		if(n==0){
			return 0;
		}
		if(n==1){
			return 1;
		}
		if(n>1){
			return sum(n-1)+sum(n-2);
		}
		return 0;
	}
	public static void main(String args[]){
		fibonacci f = new fibonacci();
		System.out.println(f.sum(3));
	}
}